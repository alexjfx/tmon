from logging import INFO
from logging import basicConfig
from logging import error
from logging import info
from os import chmod
from os import remove
from os import walk
from os.path import join
from shutil import rmtree
from stat import S_IWRITE


def __delrw__(action, name, exc=None):
    try:
        chmod(name, S_IWRITE)
        remove(name)
    except (IOError, OSError) as e:
        error('Cannot delete "{}". {}'.format(name, e))


def cleanup():
    for root, dirs, files in walk('.'):
        if '__pycache__' in dirs:
            pth = join(root, '__pycache__')
            info('Deleting "{}"...'.format(pth))
            rmtree(pth, onerror=__delrw__)
            dirs[:] = [d for d in dirs if d != '__pycache__']

        for f in files:
            if f.lower().endswith('.pyc'):
                pth = join(root, f)
                info('Deleting "{}"...'.format(pth))
                try:
                    remove(pth)
                except (IOError, OSError) as e:
                    error('Cannot delete "{}". {}'.format(pth, e))


if __name__ == '__main__':
    basicConfig(format='[%(levelname)s] %(message)s', level=INFO)
    cleanup()
