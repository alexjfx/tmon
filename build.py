from logging import INFO
from logging import basicConfig
from logging import error
from logging import info
from os import chmod
from os import listdir
from os import remove
from os import system
from os.path import exists
from os.path import splitext
from shutil import copytree
from shutil import rmtree
from stat import S_IWRITE
from sys import argv
from sys import version_info

# configurable properties
ONE_FILE = False
EXTRA_DATA = ['graphics', 'config', 'third-party']
UPX_DIR = 'C:\\upx391w'

CHECK_EXTS = {'.py', '.pyw'}
SKIP_FILES = {
    argv[0][argv[0].rfind('\\') + 1:],  # build script name
    'noname.py',  # default wxPython auto-generated file
    'pyc_cleanup.py',
    'setup.py',
}
PYI_LOC = 'C:\\Python36-32\\Scripts\\' if version_info[:2] == (3, 6) else ''

SPEC_HEADER = '''\
# -*- mode: python -*-
def extra_datas(dirs):
    if not dirs:
        return []

    from os.path import isfile
    from glob import glob

    def rec_glob(p, files):
        append = files.append
        p = p.replace('[', '[[]]').replace('?', '[?]')
        for d in glob(p):
            if isfile(d):
                append(d)
            rec_glob(d + '/*', files)

    files = []
    for dir in dirs:
        rec_glob(dir + '/*', files)
    return [(f, f, 'DATA') for f in files]


block_cipher = None

a = Analysis(
    {py_file},
    binaries=[],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher
)
pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher=block_cipher
)
'''
FOLDER_SPEC_TEMPLATE = SPEC_HEADER + '''\
exe = EXE(
    pyz,
    a.scripts,
    exclude_binaries=True,
    name='{exe_file}',
    upx={upx},
    console={console},
    icon='{icon}',
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas + extra_datas({extra_data}),
    upx={upx},
    name='{exe_file}',
)
'''
FILE_SPEC_TEMPLATE = SPEC_HEADER + '''\
exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    name='{exe_file}',
    upx={upx},
    console={console},
    icon='{icon}',
)
'''


def __delrw__(action, name, exc=None):
    try:
        chmod(name, S_IWRITE)
        remove(name)
    except (IOError, OSError) as e:
        error('Cannot delete "{}". {}'.format(name, e))


def build():
    files = listdir('.')
    scripts = [
        f for f in files
        if splitext(f)[1] in CHECK_EXTS and
        f[0] != '_' and
        not f.startswith('test') and
        f not in SKIP_FILES
    ]
    upx_exists = exists(UPX_DIR)
    for f in scripts:
        # build
        info('BUILDING {} AS {}'.format(
            f.upper(), 'ONE FILE' if ONE_FILE else 'FOLDER WITH FILES'
        ))

        # detect whether wx module imported, so use -w flag to hide the console in exe
        console = True
        for line in open(f, 'rb'):
            if line.startswith(b'from wx import App') or line.startswith(b'import wx'):
                info('wxPython import detected, app console will be hidden.')
                console = False
                break

        info('Creating spec for PyInstaller...')
        fname = splitext(f)[0]
        spec_file = fname + '.spec'
        ico_file = fname + '.ico'
        template = FILE_SPEC_TEMPLATE if ONE_FILE else FOLDER_SPEC_TEMPLATE
        with open(spec_file, 'w') as fd:
            fd.write(template.format(
                py_file=[f],
                exe_file=fname,
                upx=upx_exists,
                console=console,
                icon=ico_file if ico_file in files else '',
                extra_data=EXTRA_DATA,
            ))

        info('Launching PyInstaller...')
        cmd = '{}pyinstaller --clean --noconfirm --upx-dir={} {}'.format(
            PYI_LOC, UPX_DIR, spec_file
        )
        info(cmd)
        system(cmd)

        # copy extra data to dist dir
        if ONE_FILE:
            for d in EXTRA_DATA:
                info('Copying {0} to dist/{0}...'.format(d))
                try:
                    copytree(d, 'dist/' + d)
                except (IOError, OSError) as e:
                    error('Copy error: {}'.format(e))

        info('Cleaning up after PyInstaller...')
        __delrw__(None, spec_file)
        rmtree('build', onerror=__delrw__)

        info('----------------------------------')
    info('DONE')


if __name__ == '__main__':
    basicConfig(format='[%(levelname)s] %(message)s', level=INFO)
    build()
    input('Press ENTER to exit...')
