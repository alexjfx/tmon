from threading import Thread


class Async(Thread):

    def __init__(self, func, *args, **kwargs):
        name = kwargs.pop('name', None)
        daemon = kwargs.pop('daemon', False)
        Thread.__init__(self, target=func, name=name, args=args, kwargs=kwargs)
        self.daemon = daemon
        self.start()


class AsyncRepr:

    def __repr__(self):
        return '%s <0x%08X>' % (self.__name__, id(self))


def get_param(name, args, kwargs, default=None):
    try:
        return kwargs.pop(name)
    except KeyError:
        return getattr(args[0], name, default) if args else default
