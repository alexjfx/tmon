from Queue import Empty
from Queue import PriorityQueue
from lib.async import Async
from lib.async import AsyncRepr
from lib.async.priorities import __STOP__
from lib.async.task import Task
from lib.async.worker import Worker
from logging import debug
from threading import Lock


class TaskScheduler(AsyncRepr):

    def __init__(self, workers_count=4, auto_start=True, name=None):
        self.__name__ = name or 'Scheduler'
        self.__lock = Lock()
        self.__queue__ = PriorityQueue()
        self.wrcount = workers_count
        self.workers = None

        if auto_start:
            self.start()

    def __can_schedule(self, task):
        if task.__skipif__ is None:
            return True
        _, args, kwargs = task.__fndata__
        compare = task.__skipif__
        task_name = task.__name__
        task_value = compare(*args, **kwargs)
        for _, tsk in self.__queue__.queue[:]:
            if tsk.__name__ != task_name:
                continue
            _, args, kwargs = tsk.__fndata__
            if compare(*args, **kwargs) == task_value:
                debug('%r with value %r cannot be scheduled twice.' % (task, task_value))
                return False
        return True

    def schedule(self, task):
        if task is None:
            self.__queue__.put((__STOP__, None))
        elif isinstance(task, Task) and self.__can_schedule(task):
            task.stopped.clear()
            task.idle.clear()
            self.__queue__.put((task.priority, task))

    def is_started(self):
        return self.workers is not None

    def workers_available(self):
        for worker in self.workers or []:
            if not worker.is_busy:
                return True
        return False

    def scheduled_tasks(self):
        return [task for _, task in self.__queue__.queue]

    def start(self):
        with self.__lock:
            if self.is_started():
                debug('%r already started.' % self)
                return False
            c = self.wrcount
            self.workers = [
                Worker(self, name='%r; Worker-%d/%d' % (self, i + 1, c)) for i in range(c)
            ]
            debug('%r started.' % self)
            return True

    def __asyncjoin(self):
        [wr.join() for wr in self.workers]
        self.workers = None
        qget = self.__queue__.get_nowait
        qtdone = self.__queue__.task_done
        try:
            while True:
                _, task = qget()
                qtdone()
                task.idle.set()
                task.stopped.set()
                debug('Removed %r from %r.' % (task, self))
        except Empty:
            pass
        debug('%r stopped.' % self)
        self.__lock.release()

    def stop(self):
        self.__lock.acquire()
        if not self.is_started():
            debug('%r already stopped.' % self)
            try:
                return Async(lambda: None)
            finally:
                self.__lock.release()
        schedule = self.schedule
        [schedule(None) for i in range(self.wrcount)]
        return Async(self.__asyncjoin, name='AsyncSchedulerStop')
