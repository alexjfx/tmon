from functools import wraps
from lib.async import get_param
from threading import _RLock
from threading import _active


class ReentrantLock(_RLock):

    @property
    def acquired(self):
        return self._RLock__count > 0

    @property
    def owner(self):
        owner_tid = self._RLock__owner
        try:
            return _active[owner_tid].name
        except KeyError:
            return '' if owner_tid is None else str(owner_tid)


def get_lock(blocking=True, failure_cbk=None, name='lock'):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            lock = get_param(name, args, kwargs)
            if not lock.acquire(blocking):
                if callable(failure_cbk):
                    kwargs[name] = lock
                    return failure_cbk(*args, **kwargs)
                return
            try:
                return func(*args, **kwargs)
            finally:
                lock.release()
        return wrapper
    return decorator
