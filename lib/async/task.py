from functools import partial
from functools import wraps
from lib.async import AsyncRepr
from lib.async import get_param
from lib.async.priorities import NORMAL_PRIORITY
from logging import debug
from logging import error
from threading import Event

__TASK = '__task__'
__STUB__ = lambda *args, **kwargs: None


class __SIGTERM__(Exception):
    pass


class Task(AsyncRepr):

    def __init__(self, func, args=(), kwargs=None, priority=NORMAL_PRIORITY, interval=None,
                 scheduler=None, name=None, callbacks=None, skip_if_found=None):
        kwargs = kwargs or {}
        callbacks = callbacks or {}
        get = callbacks.get
        self.on_sigterm = get('on_sigterm', __STUB__)
        self.on_error = get('on_error', __STUB__)
        self.on_done = get('on_done', __STUB__)

        self.__name__ = name or func.__name__
        self.__fndata__ = func, args, kwargs
        self.__skipif__ = skip_if_found
        self.__sched = scheduler

        self.priority = priority
        self.interval = interval
        self.idle = Event()
        self.stopped = Event()
        self.idle.set()

    def get_func(self):
        return self.__fndata__

    def get_name(self):
        return self.__name__

    # returns from task func if task is stopped
    def return_on_stop(self):
        if self.stopped.is_set():
            debug('__SIGTERM__ in %r' % self)
            raise __SIGTERM__

    def is_idle(self):
        return self.idle.is_set()

    def is_stopped(self):
        return self.stopped.is_set()

    def wait(self):
        self.idle.wait()

    def start(self):
        sched = self.__sched
        try:
            sched.schedule(self)
        except AttributeError:
            error('Cannot start %r because %r is not a scheduler.' % (self, sched))

    def stop(self, wait=False):
        self.stopped.set()
        if wait:
            self.idle.wait()


def task(func=None, priority=NORMAL_PRIORITY, skip_if_found=None):
    if func is None:
        return partial(task, priority=priority, skip_if_found=skip_if_found)
    elif isinstance(func, int):
        return partial(task, priority=func, skip_if_found=skip_if_found)

    @wraps(func)
    def wrapper(*args, **kwargs):
        ival = get_param('interval', args, kwargs)
        sched = get_param('scheduler', args, kwargs)
        tsk = Task(func, args, kwargs, priority, ival, sched, skip_if_found=skip_if_found)
        setattr(wrapper, __TASK, tsk)
        tsk.start()
        return tsk
    setattr(wrapper, __TASK, True)
    return wrapper


def istask(obj):
    return bool(getattr(obj, __TASK, None))


def TASK(obj):
    return obj if isinstance(obj, Task) else getattr(obj, __TASK, None)


def ROTS(obj):
    return getattr(TASK(obj), 'return_on_stop', None)


def interruptible(iterable, task_or_rots):
    rots = ROTS(task_or_rots)
    if rots is None:
        rots = task_or_rots if callable(task_or_rots) else lambda: None
    for x in iterable:
        rots()
        yield x
