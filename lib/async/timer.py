from lib.async.scheduler import TaskScheduler
from lib.async.task import Task
from lib.async.task import __STUB__


class Timer(object):

    def __init__(self, interval=None, auto_start=False, callbacks=None):
        self.__sched__ = TaskScheduler(1, False, self.__class__.__name__)
        self.__task__ = Task(self.handler, interval=interval, callbacks=callbacks or {})
        if auto_start:
            self.start()

    def handler(self):
        raise NotImplementedError

    @property
    def started(self):
        return self.__sched__.is_started()

    @property
    def interval(self):
        return self.__task__.interval

    @interval.setter
    def interval(self, value):
        self.__task__.interval = value

    def start(self):
        if self.__sched__.start():
            self.__sched__.schedule(self.__task__)

    def stop(self, wait=False):
        self.__task__.stop()
        thr = self.__sched__.stop()
        if wait:
            thr.join()


class Monitor(Timer):

    def __init__(self, trunk, process_func=None, **kwargs):
        Timer.__init__(self, **kwargs)
        self.lock = trunk.lock
        self.trunk = trunk
        self.process_func = process_func or __STUB__
