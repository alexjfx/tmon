from lib.async import Async
from lib.async.priorities import __STOP__
from lib.async.task import __SIGTERM__
from logging import debug
from logging import exception


class Worker(Async):

    def __init__(self, scheduler, name=None):
        self.__busy = False
        self.__sched__ = scheduler
        Async.__init__(self, self.__handler, name=name)

    def __handler(self):
        scheduler = self.__sched__
        qget = scheduler.__queue__.get
        qtdone = scheduler.__queue__.task_done
        schedule = scheduler.schedule
        for _, task in iter(qget, (__STOP__, None)):
            self.__busy = True
            if task.interval is None:
                debug('Received %r.' % task)
            try:
                func, args, kwargs = task.__fndata__
                res = None
                try:
                    # TODO: handle generators in future
                    res = func(*args, **kwargs)
                except __SIGTERM__:
                    debug('Terminated %r.' % task)
                    task.on_sigterm(*args, **kwargs)
                except Exception as e:
                    exception(e)
                    task.on_error(*args, **kwargs)
                finally:
                    task.on_done(res, *args, **kwargs)
            except Exception as e:
                exception(e)
            finally:
                qtdone()
                task.idle.set()
            if task.interval is None:
                task.stopped.set()
            elif not task.stopped.wait(task.interval):
                schedule(task)
            self.__busy = False
        qtdone()
        debug('Worker stopped.')

    @property
    def is_busy(self):
        return self.__busy
