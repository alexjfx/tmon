from lib.bindings.logic import *
from lib.gui.consts import BITBUCKET
from lib.gui.consts import CCOLLAB
from lib.rest.bitbucket import BitbucketSession
from lib.review.bitbucket import BitbucketMonitor
from lib.review.ccollab import CcollabMonitor
from lib.review.stub import StubMonitor
from lib.vcs.git import GitCommands
from lib.vcs.svn import SvnCommands

VCS_REVIEW_BINDING = BindingSpec(
    Bind(SvnCommands, CcollabMonitor, srv=CCOLLAB),
    # TODO: pass SyncBitbucketSession in rest and use rest in BitbucketMonitor and other places
    Bind(GitCommands, BitbucketMonitor, srv=BITBUCKET, entire_review=True, rest=BitbucketSession),
    Bind(DEFAULT, StubMonitor),
)

SSH_BINDING = {
    BitbucketSession: BITBUCKET,
}
