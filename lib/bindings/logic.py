from logging import debug

DEFAULT = '__default__'


class Bind:

    def __init__(self, key=None, cls=None, **attrs):
        self.key = key
        self.cls = cls
        self.__dict__.update(attrs)

    def __getattr__(self, name):
        return self.__dict__.get(name)


class BindingSpec:

    def __init__(self, *bindings):
        self.__default = Bind(DEFAULT)
        self.__spec = spec = {}
        for b in bindings:
            if b.key == DEFAULT:
                self.__default = b
            else:
                spec[b.key] = b

    def __iter__(self):
        for k, v in self.__spec.iteritems():
            yield k, v

    def __from_ins(self, obj):
        for key, binding in self:
            if isinstance(obj, key):
                return binding

    def __from_str(self, string):
        string = string.lower()
        for key, binding in self:
            if key in string:
                return binding

    def __getitem__(self, obj):
        if isinstance(obj, (str, unicode)):
            binding = self.__from_str(obj)
        else:
            binding = self.__from_ins(obj)

        if binding is not None:
            return binding

        debug('No binding for "{}". Default binding will be used.'.format(obj))
        return self.__default
