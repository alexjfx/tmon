from lib.async.lock import ReentrantLock
from lib.async.lock import get_lock
from lib.cmdline import cmd
from lib.common import make_dirs
from lib.logger import log
from logging import error
from os import environ
from os.path import exists
from os.path import expanduser

SSH_KEY_DIR = expanduser('~') + '/.ssh'
SSH_KEY_FILE = SSH_KEY_DIR + '/id_rsa'
PUB_KEY_FILE = SSH_KEY_DIR + '/id_rsa.pub'
SSH_CFG_FILE = SSH_KEY_DIR + '/config'


class SSHConnection:

    def __init__(self):
        self.lock = ReentrantLock()
        self.__conn = {}

    @get_lock()
    def __getitem__(self, server):
        return self.__conn.get(server, False)

    @get_lock()
    def valid(self, server):
        check_str = 'ssh -T git@' + server.replace('https://', '')
        self.__conn[server] = res = cmd(
            check_str, timeout=3, tmsg='Timeout expired for "{}".'.format(check_str),
        )
        return res

    @get_lock()
    @log(
        'Generating SSH keys...',
        based_on_return=True,
        true='Generated new SSH keys.',
        false='Cannot generate SSH keys.',
    )
    def generate_keys(self):
        user = '{USERDOMAIN}+{USERNAME}@{COMPUTERNAME}'.format(**environ)
        command = 'ssh-keygen -t rsa -C "{}" -N "" -f {}'.format(user, SSH_KEY_FILE)

        make_dirs(SSH_KEY_DIR)
        if cmd(command):
            return True
        else:
            self.__conn = {}

    @get_lock()
    @log(
        'Configuring local SSH settings...',
        based_on_return=True,
        true='Configured local SSH settings.',
        false='Cannot configure local SSH settings.',
    )
    def __write_config(self):
        try:
            with open(SSH_CFG_FILE, 'wb+') as fd:
                fd.write('\n\t'.join([
                    'Host *',
                    'StrictHostKeyChecking no',
                    'PasswordAuthentication no'
                ]))
            return True
        except (IOError, OSError) as e:
            error('Cannot write to "{}": {}'.format(SSH_CFG_FILE, e))

    @get_lock()
    @log('Setting up SSH connection...', 'Completed SSH connection setup.')
    def setup(self, backends):
        if not exists(SSH_KEY_FILE) and not self.generate_keys():
            return
        if not exists(PUB_KEY_FILE):
            error('Cannot set up SSH due to missing file: {}'.format(PUB_KEY_FILE))
            self.__conn = {}
            return
        self.__write_config()
        valid = self.valid
        for cls, srv, auth in backends:
            if valid(srv):
                continue
            with cls(srv, auth) as session:
                self.__conn[srv] = session.register_ssh_key(PUB_KEY_FILE)


ssh_connection = SSHConnection()
