try:
    from Levenshtein import ratio
except ImportError:
    ratio = None
from logging import warning
from re import IGNORECASE
from re import UNICODE
from re import compile
from re import escape


def get_best_phrase(string, words, start, end):
    best_phrase = '', 0, 0
    word_count = 1
    while start + word_count <= end:
        phrase = ' '.join(words[start:start + word_count])
        r = int(ratio(string, phrase) * 100)
        if r > best_phrase[2]:
            best_phrase = phrase, word_count, r
        word_count += 1
    return best_phrase


class Matcher:

    def __init__(self, string, match_case=False, whole_word=False, reg_exp=False, mpc=100):
        assert string, 'String cannot be empty.'
        assert isinstance(mpc, int), 'mpc must be int.'
        self.string = string if match_case else string.lower()
        self.match_case = match_case
        self.whole_word = whole_word
        self.reg_exp = reg_exp
        self.mpc = mpc
        if whole_word or reg_exp:
            if not reg_exp:
                string = escape(string)
            pattern = ur'\b{}\b'.format(string) if whole_word else string
            self.regex_obj = compile(pattern, UNICODE if match_case else UNICODE | IGNORECASE)

    def __findall(self, line):
        start = 0
        string = self.string
        inc = len(string)
        while True:
            start = line.find(string, start)
            if start == -1:
                return
            yield start, start + inc, None
            start += inc

    def __lst_findall(self, line):
        string = self.string
        mpc = self.mpc

        words = line.split(' ')
        i = 0
        end = len(words)
        while i <= end:
            phrase, word_count, r = get_best_phrase(string, words, i, end)
            if r < mpc or not words[i]:
                i += 1
            else:
                start = len(' '.join(words[:i])) + 1 if i else 0
                yield start, start + len(phrase), r
                i += word_count

    def __call__(self, line):
        if line == '':
            return
        if self.whole_word or self.reg_exp:
            return [(m.start(), m.end(), None) for m in self.regex_obj.finditer(line)]
        if self.mpc == 100:
            find = self.__findall
        elif ratio is not None:
            find = self.__lst_findall
        else:
            find = self.__findall
            warning(
                'Unexact search is not possible because the Levenshtein module is not imported. '
                'Simple search will be performed instead.'
            )
        return list(find(line if self.match_case else line.lower()))


def splitter(string, positions):
    last = 0
    for start, end, mpc in positions:
        yield string[last:start]
        yield string[start:end], mpc
        last = end
    yield string[last:len(string)]


if __name__ == '__main__':
    string = '''from consts import *'''
    m = Matcher('match context', mpc=50)
    res = m(string)
    for start, end, mpc in res:
        print '"%s"' % string[start:end], mpc
    print list(splitter(string, res))
