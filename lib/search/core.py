from lib.async.task import interruptible
from lib.common import cached
from lib.common import match_file_pattern
from multiprocessing import Pool
from os.path import sep
from scandir import walk

# ignore some binary files
IGNORE_FILE_EXTS = [
    # binaries
    '.dll', '.exe', '.lib', '.com', '.msi', '.jar', '.pdb', '.so',
    # graphics
    '.png', '.gif', '.jpg', '.bmp', '.ico',
    # documents
    '.pdf', '.chm',
    # archives
    '.zip', '.7z',
]

CHAR_COUNT = 1000


def __utf8reader(file_path):
    lines = []
    add_line = lines.append
    try:
        with open(file_path, 'rb') as fd:
            for num, line in enumerate(fd, 1):
                try:
                    line = line.decode('utf8').rstrip()
                except UnicodeDecodeError:
                    continue
                if line == '':
                    add_line({'num': num, 'part': 1, 'str': '', })
                    continue
                for part, i in enumerate(xrange(0, len(line), CHAR_COUNT), 1):
                    add_line({'num': num, 'part': part, 'str': line[i:i + CHAR_COUNT], })
    except (OSError, IOError):
        pass
    return lines


def __context(idx, lines, count=2):
    return lines[0 if count > idx else idx - count:idx + count + 1]


def __procf((file_obj, matcher, context)):
    matches_found = 0
    lines = []
    add_line = lines.append
    file_data = __utf8reader(file_obj['file'])
    for idx, line in enumerate(file_data):
        positions = matcher(line['str'])
        if positions:
            add_line((line, positions, __context(idx, file_data) if context else None))
            matches_found += len(positions)
    if matches_found > 0:
        return {
            'file_obj': file_obj,
            'matches_found': matches_found,
            'lines': lines,
        }


def __walker(path, ignores, all_files, check_files, task):
    for root, dirs, files in interruptible(walk(path), task):
        dirs[:] = [d for d in dirs if d not in ignores]
        if all_files:
            file_list = (
                f for f, lf in ((fn, fn.lower()) for fn in files)
                if not any(lf.endswith(ext) for ext in IGNORE_FILE_EXTS)
            )
        else:
            file_list = (f for f in files if match_file_pattern(f, check_files, True))

        for pth in (root + sep + f for f in file_list):
            yield pth


@cached(lambda trunk, all_files=False, check_files=None, task=None: (
    trunk.last_rev, trunk.ignore_dirs, all_files, check_files or trunk.check_files
))
def walk_files(trunk, all_files=False, check_files=None, task=None):
    return list(__walker(
        trunk.last_rev,
        set(trunk.ignore_dirs.split(', ')),
        all_files, check_files or trunk.check_files,
        task,
    ))


def search_text(file_list, matcher, context=True):
    pool = Pool()
    try:
        for data in pool.imap_unordered(__procf, ((f, matcher, context) for f in file_list)):
            yield data
    finally:
        pool.close()
