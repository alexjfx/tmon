from functools import partial
from functools import wraps
from inspect import getargspec
from logging import StreamHandler
from logging import debug
from logging import error
from logging import getLevelName
from logging import getLogger
from logging import info
from logging import warning
from logging.config import dictConfig
from wx import CallAfter

AS_INFO = info
AS_DEBUG = debug
AS_ERROR = error
AS_WARNING = warning


class WxConsoleHandler(StreamHandler):

    def __init__(self, log_func, level='INFO'):
        StreamHandler.__init__(self)
        self.level = getLevelName(level) if isinstance(level, (str, unicode)) else level
        self.emit = partial(CallAfter, log_func)


def init_logger(output_file, console_level='INFO'):
    dictConfig({
        'version': 1,
        'handlers': {
            'file': {
                'class': 'logging.FileHandler',
                'formatter': 'default',
                'filename': output_file,
            },
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'default',
                'level': console_level,
            },
        },
        'loggers': {
            '': {
                'handlers': ['file', 'console'],
                'level': 'DEBUG',
            },
        },
        'formatters': {
            'default': {
                'format': ' | '.join([
                    '%(asctime)s',
                    '%(levelname)7s',
                    '%(threadName)s #%(thread)d',
                    '%(module)s, %(lineno)s',
                    '%(message)s',
                ]),
                'datefmt': '%d.%m.%Y %H:%M:%S',
            },
        },
    })


def register_wxconsole(log_func):
    getLogger().addHandler(WxConsoleHandler(log_func, getLogger().handlers[1].level))


def __fmtlog(fn, msg, dct):
    try:
        msg = msg.format(**dct)
    except KeyError:
        pass
    except AttributeError:
        return
    fn(msg)


def __fmtreq(*messages):
    return any(v for v in messages if v is not None and '{' in v and '}' in v)


def __logfunc(data, default_func):
    if not isinstance(data, tuple):
        return default_func, data
    return data[:2]


def log(invoke=None, complete=None, true=None, false=None, based_on_return=False, __info=AS_INFO):
    if based_on_return:
        t_log, t_msg = __logfunc(true, __info)
        f_log, f_msg = __logfunc(false, AS_ERROR)
        fmt_required = __fmtreq(invoke, t_msg, f_msg)
    else:
        fmt_required = __fmtreq(invoke, complete)

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            arg_dict = {}
            if fmt_required:
                f_args, _, _, f_defaults = getargspec(func)
                if f_defaults is not None:
                    arg_dict = dict(zip(f_args[-len(f_defaults):], f_defaults))
                arg_dict.update(dict(zip(f_args, args) + kwargs.items()))
                arg_dict['__func__'] = func

            __fmtlog(__info, invoke, arg_dict)
            arg_dict['__result__'] = res = func(*args, **kwargs)
            if not based_on_return:
                __fmtlog(__info, complete, arg_dict)
            else:
                if res:
                    __fmtlog(t_log, t_msg, arg_dict)
                else:
                    __fmtlog(f_log, f_msg, arg_dict)

            return res
        return wrapper
    return decorator


debug_log = partial(log, __info=AS_DEBUG)
