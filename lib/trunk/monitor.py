from lib.async.lock import get_lock
from lib.async.task import ROTS
from lib.async.task import interruptible
from lib.async.timer import Monitor
from lib.common import match_file_pattern
from logging import error
from logging import info
from os.path import sep
from watchdog.events import EVENT_TYPE_DELETED
from watchdog.events import EVENT_TYPE_MOVED
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


def ignores_set(ignore_dirs):
    return {sep + d + sep for d in ignore_dirs.split(', ')}


class TrunkMonitor(Monitor):

    class __FileHandler(FileSystemEventHandler):

        def __init__(self, trunk, process_func):
            self.tr = trunk
            self.fn = process_func
            self.set_ignore_dirs()

        def set_ignore_dirs(self):
            self.__curid_str = dirs = self.tr.ignore_dirs
            self.ignores = ignores_set(dirs)

        def dispatch(self, event):
            if event.is_directory:
                return
            pth = event.src_path

            # update ignores in case it changed in trunk
            if self.__curid_str != self.tr.ignore_dirs:
                self.set_ignore_dirs()
            # check for ignore dir match
            for d in self.ignores:
                if d in pth:
                    return

            info('{}: {}'.format(event.event_type.capitalize(), pth))
            self.fn(pth, deleted=event.event_type in {EVENT_TYPE_DELETED, EVENT_TYPE_MOVED})

    def __init__(self, trunk, **kwargs):
        Monitor.__init__(self, trunk, **kwargs)
        self.__observer = None

    def start_observer(self):
        if self.__observer is not None:
            return
        trunk = self.trunk
        pth = trunk.last_rev
        if pth is None:
            error('Cannot monitor trunk "{}" without local repository.'.format(trunk.url))
            return
        self.__observer = observer = Observer()
        observer.name = 'FileSystemObserver'
        observer.schedule(self.__FileHandler(trunk, self.process_func), pth, recursive=True)
        observer.start()
        info('Started watching for local file changes in trunk "{}".'.format(trunk.url))

    def stop_observer(self):
        observer = self.__observer
        if observer is None:
            return
        observer.stop()
        observer.join()
        self.__observer = None
        info('Stopped watching for local file changes in trunk "{}".'.format(self.trunk.url))

    def stop(self, wait=False):
        self.stop_observer()
        Monitor.stop(self, wait)

    @get_lock()
    def handler(self):
        trunk = self.trunk
        last_rev = trunk.last_rev
        if not last_rev:
            # TODO: start checkout!
            error('Cannot update trunk "{}" without local repository.'.format(trunk.url))
            return

        url = trunk.url
        return_on_monitor_stop = ROTS(self)
        return_on_monitor_stop()

        # stop observer
        self.stop_observer()
        return_on_monitor_stop()

        def iter_files(iterable):
            ignores = ignores_set(trunk.ignore_dirs)
            for x in interruptible(iterable, return_on_monitor_stop):
                pth = x[-1] if isinstance(x, tuple) else x
                for d in ignores:
                    if d in pth:
                        continue
                yield x

        # update trunk
        info('Updating trunk "{}"...'.format(url))
        process_func = self.process_func
        check_files = trunk.check_files
        updated_files = set()
        for st, pth in iter_files(trunk.vcs.UPDATE()):
            __del = st == 'D'
            if __del or match_file_pattern(pth, check_files):
                process_func(pth, deleted=__del)
                updated_files.add(pth)
        info('Trunk "{}" is up-to-date.'.format(url))

        # check required in case of changes to files during update
        # because file observer does not work during update
        # also required on first run of handler
        info('Checking modified files in trunk "{}"...'.format(url))
        for pth in iter_files(trunk.vcs.MODIFIED(last_rev)):
            if pth not in updated_files:
                process_func(pth, modified=True)
                updated_files.add(pth)
        info('Modified files in trunk "{}" checked.'.format(url))

        # start observer
        return_on_monitor_stop()
        self.start_observer()

        return updated_files
