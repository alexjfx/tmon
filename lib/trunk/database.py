from lib.async.lock import get_lock
from lib.common import Representable
from lib.common import SaveableDict
from threading import Lock


class TrunkDatabase(Representable):

    def __init__(self, db_file):
        self.lock = Lock()
        self.__trunks = SaveableDict(db_file)

    @get_lock()
    def save(self):
        self.__trunks.save()

    @get_lock()
    def add(self, trunk):
        url = trunk.url
        if url in self.__trunks:
            return False
        self.__trunks[url] = trunk
        return True

    @get_lock()
    def __iter__(self):
        for tr in sorted(self.__trunks.itervalues(), key=lambda tr: tr.url):
            yield tr

    @get_lock()
    def __contains__(self, url):
        return url in self.__trunks

    @get_lock()
    def __getitem__(self, url):
        return self.__trunks.get(url)

    @get_lock()
    def __delitem__(self, trunk):
        try:
            del self.__trunks[trunk.url]
        except KeyError:
            pass
