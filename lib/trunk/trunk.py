from filecmp import cmp
from lib.async import Async
from lib.async.lock import ReentrantLock
from lib.async.lock import get_lock
from lib.async.task import interruptible
from lib.common import Representable
from lib.common import delete_dir
from lib.common import hexhash
from lib.common import join_paths
from lib.common import make_dirs
from lib.common import match_file_pattern
from lib.common import touch
from lib.vcs import init_vcs
from logging import info
from os.path import exists
from os.path import sep
from scandir import walk

INIT_REV = 'initial'
LAST_REV = 'last'


class PathProperty(object):

    class value:

        def __init__(self, pth=None):
            self.__pth = pth if pth is None else pth

        def __call__(self):
            return self.__pth

    def __init__(self, name):
        self.__name = name

    def __get__(self, obj, typ):
        if obj is None:
            return self
        pth = getattr(obj, self.__name, None)
        return pth if pth is None else make_dirs(pth)

    def __set__(self, obj, val):
        if isinstance(val, self.__class__.value):
            setattr(obj, self.__name, val())

    def __delete__(self, obj):
        pass


class Trunk(Representable):

    local_rep = PathProperty('__lrep__')
    init_rev = PathProperty('__irev__')
    last_rev = PathProperty('__lrev__')

    def __init__(self, url, branch=None, check_files='', ignore_dirs='', reviewer='',
                 auto_diff=True):
        assert url, 'Trunk URL cannot be empty.'
        self.__url = url
        self.__md5hash = hexhash(url)
        self.lock = ReentrantLock()

        self.branch = branch
        self.check_files = check_files
        self.ignore_dirs = ignore_dirs
        self.reviewer = reviewer
        self.auto_diff = auto_diff
        init_vcs(self)

        # for use by ReviewMonitor
        self.__reviews__ = {}

    def __hash__(self):
        try:
            return self.__hash
        except AttributeError:
            self.__hash = h = hash(self.__url)
            return h

    def __eq__(self, other):
        return self.__url == other.url if isinstance(other, Trunk) else False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __getstate__(self):
        return {k: v for k, v in self.__dict__.iteritems() if k not in {'lock', 'vcs'}}

    def __setstate__(self, dct):
        self.__dict__ = dct
        self.lock = ReentrantLock()
        init_vcs(self)

    @property
    def md5hash(self):
        return self.__md5hash

    @property
    def url(self):
        return self.__url

    def irev_pth(self, lrev_pth):
        pth = lrev_pth.replace(self.__lrev__, self.__irev__, 1)
        return pth if exists(pth) else touch(pth)

    def shorten(self, pth):
        return pth.replace(self.__lrev__, '', 1).replace('\\', '/')

    def expand(self, pth):
        return join_paths(self.__lrev__, pth)

    @get_lock()
    def set_local_rep(self, pth=None):
        old_rep = self.local_rep
        if old_rep == pth:
            return

        self.__reviews__.clear()
        url = self.__url
        md5hash = self.__md5hash

        info('Setting local rep. of trunk "%s" to "%s"...' % (url, pth or ''))
        self.local_rep = PathProperty.value(pth)
        if pth is None:
            self.init_rev = self.last_rev = PathProperty.value()
        else:
            self.init_rev = PathProperty.value(join_paths(pth, md5hash, INIT_REV))
            self.last_rev = PathProperty.value(join_paths(pth, md5hash, LAST_REV))
            irev = self.init_rev
            lrev = self.last_rev
        info('Local rep. of trunk "%s" is now "%s".' % (url, pth or ''))

        dt = None
        if old_rep is not None:
            info('Deleting trunk "%s" from local rep. "%s"...' % (url, old_rep))
            dt = Async(delete_dir, join_paths(old_rep, md5hash), name='RmTree')

        if pth is not None:
            vcs = self.vcs
            info('Downloading last rev. of trunk "%s" to local rep. "%s"...' % (url, pth))
            lrt = Async(vcs.DOWNLOAD, lrev, name='LastRevDownload')
            if vcs.IS_VALID(irev):
                info('Init. rev. of trunk "%s" already exists in local rep. "%s".' % (url, pth))
            else:
                info('Downloading init. rev. of trunk "%s" to local rep. "%s"...' % (url, pth))
                Async(vcs.DOWNLOAD, irev, name='InitRevDownload').join()
                info('Init. rev of trunk "%s" downloaded.' % url)
            lrt.join()
            info('Last rev. of trunk "%s" downloaded.' % url)

        if dt is not None:
            dt.join()
            info('Trunk "%s" deleted from local rep. "%s".' % (url, old_rep))

    def diff(self, task_rots=None):
        right_pth = self.last_rev
        left_pth = self.init_rev
        check_files = self.check_files
        if not right_pth or not left_pth or not check_files:
            return
        ignores = set(self.ignore_dirs.split(', '))

        for right_root, dirs, files in interruptible(walk(right_pth), task_rots):
            dirs[:] = [d for d in dirs if d not in ignores]
            file_list = [f for f in files if match_file_pattern(f, check_files, True)]
            if not file_list:
                continue
            left_root = right_root.replace(right_pth, left_pth, 1)
            for f in file_list:
                left = left_root + sep + f
                right = right_root + sep + f
                if not exists(left) or not cmp(left, right):
                    yield right
