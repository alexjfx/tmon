import sys
import wx

from lib.async import Async
from lib.async.scheduler import TaskScheduler
from lib.async.task import TASK
from lib.async.task import task
from lib.bindings import SSH_BINDING
from lib.cmdline import CMD_ARGS
from lib.common import SaveableDict
from lib.common import join_paths
from lib.gui.__icon__ import AppIcon
from lib.gui.consts import *
from lib.gui.dialogs.log_dialog import LogDialog
from lib.gui.dialogs.settings_dialog import SettingsDialog
from lib.gui.diffctrls.identifiers import *
from lib.gui.diffctrls.main_toolbar import MainToolBar
from lib.gui.diffctrls.notebook import DiffNoteBook
from lib.gui.tray import TrayIcon
from lib.gui.utils.common import BusyInfo
from lib.gui.utils.inheritable import inheritables
from lib.registry import Registry
from lib.ssh import ssh_connection
from lib.trunk.database import TrunkDatabase
from wx.lib.agw.infobar import InfoBar


@inheritables('tray', 'nbook', 'trunks', 'settings', 'scheduler', 'main_frame', 'main_tlbar',
              'main_info', 'log')
class MainFrame(wx.Frame):

    def __init__(self, parent, title='TrunkMon'):
        self.scheduler = TaskScheduler(name='MainFrameTasks')
        self.settings = settings = SaveableDict(SETTINGS_DB, {
            TMON: 3600,
            RMON: 60,
            CREDS: ('', ''),
            CCOLLAB: 'http://review',
            BITBUCKET: 'https://git.acronis.com',
            DIFF_TOOL: join_paths(THIRDPARTY_DIR, 'meld/meld.exe'),
            EDIT_TOOL: join_paths(THIRDPARTY_DIR, 'sublime/sublime_text.exe'),
            TERM_TOOL: '',
            FRAME_SIZE: (550, 400),
            FRAME_ICON: False,
            FRAME_POS: None,
            ACTIVE_PAGE: None,
            SVN_BLAME: 'tortoiseproc /command:blame /path:"{}"',
            GIT_BLAME: 'git gui blame "{}"',
            SVN_LOG: 'tortoiseproc /command:log /path:"{}"',
            GIT_LOG: 'gitk --all "{}"',
        })
        self.__IconizeOnClose__ = True

        wx.Frame.__init__(
            self, parent, title=title, size=settings[FRAME_SIZE],
            style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL
        )
        self.SetIcon(AppIcon.GetIcon())
        self.SetSizeHintsSz(wx.Size(500, 300))
        self.SetFont(wx.Font(9, 75, 90, 90, False, 'Consolas'))
        self.Center()

        self.log = LogDialog(self)
        self.tray = TrayIcon(self)
        self.main_frame = self
        self.main_tlbar = MainToolBar(self)
        self.main_info = InfoBar(self)
        self.nbook = nbook = DiffNoteBook(self)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.main_info, 0, wx.EXPAND)
        sizer.Add(self.main_tlbar, 0, wx.EXPAND)
        sizer.Add(self.nbook, 1, wx.EXPAND)
        self.SetSizer(sizer)
        self.Layout()

        if settings[FRAME_POS] is None:
            self.ConfigureSettings()
        else:
            self.SetPosition(settings[FRAME_POS])
        if settings[FRAME_ICON]:
            self.Iconize()

        self.ConfigureSSHConnection()
        CMD_ARGS['USER'], CMD_ARGS['PASS'] = settings[CREDS]
        CMD_ARGS['USER'] = [('svn', '--non-interactive --no-auth-cache --username')]
        CMD_ARGS['PASS'] = [('svn', '--password')]

        self.trunks = TrunkDatabase(TRUNKS_DB)
        active_page = settings[ACTIVE_PAGE]
        for trunk in self.trunks:
            panel = nbook.CreatePage(trunk, trunk.url == active_page)
            panel.StartMonitors()
        self.GUILocker(interval=0.1)

        set_evt = self.Bind
        set_evt(wx.EVT_ICONIZE, self.__OnIconize)
        set_evt(wx.EVT_CLOSE, self.__OnClose)

    @task
    def GUILocker(self):
        panel = self.nbook.GetCurPage()
        if not panel:
            return

        trunk = panel.trunk
        unlocked = not trunk.lock.acquired
        difftask_idle = panel.tlbar.DiffTask.is_idle()
        selitem = panel.dlist.SelectedItem
        selected = bool(selitem)
        workers_available = self.scheduler.workers_available()

        acceptable = unlocked and bool(trunk.check_files) and selected
        modified = unlocked and selected and selitem[STATE_COL] == MODIFIED

        panel.stbar[LOCK_FIELD] = trunk
        self.main_tlbar.EnableItems({
            ADDTRUNK_TOOL_ID: workers_available,
            DELTRUNK_TOOL_ID: unlocked and workers_available,
        })
        panel.tlbar.EnableItems({
            STARTDIFF_TOOL_ID: unlocked and difftask_idle,
            STOPDIFF_TOOL_ID: not difftask_idle,
            BRANCH_TOOL_ID: unlocked,
            UPDATE_TOOL_ID: unlocked,
            RESET_TOOL_ID: unlocked,
            ACCEPT_TOOL_ID: acceptable,
            DECLINE_TOOL_ID: acceptable,
            REVIEW_TOOL_ID: modified,
            COMMIT_TOOL_ID: modified,
            REVERT_TOOL_ID: modified,
        })

    def ConfigureSSHConnection(self):
        with BusyInfo(self, 'Configuring SSH...'):
            settings = self.settings
            creds = settings[CREDS]
            servers = [(cls, settings[sn]) for cls, sn in SSH_BINDING.iteritems()]
            old_ssh = {srv: ssh_connection[srv] for _, srv in servers}
            ssh_connection.setup((cls, srv, creds) for cls, srv in servers)
            new_ssh = {srv: ssh_connection[srv] for _, srv in servers}
            return old_ssh != new_ssh

    def ConfigureSettings(self, event=None):
        settings = self.settings
        old_auth = settings[CREDS], settings[BITBUCKET], settings[CCOLLAB]
        with SettingsDialog(self) as dlg:
            app_name = self.Title
            if event is None:
                dlg.NoteBook.SetSelection(1)

            reg = Registry('SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run')
            try:
                dlg.AutoStartCbx.SetValue(reg.exists(app_name))
                if dlg.ShowModal() == wx.ID_CANCEL:
                    return
                if dlg.AutoStartCbx.GetValue():
                    reg.set(app_name, sys.argv[0])
                else:
                    reg.delete(app_name)
            finally:
                reg.close()

        if event is None:
            return

        CMD_ARGS['USER'], CMD_ARGS['PASS'] = settings[CREDS]
        ssh_changed = self.ConfigureSSHConnection()
        auth_changed = (old_auth[0] != settings[CREDS]) or ssh_changed
        srv_changed = old_auth[1:] != (settings[BITBUCKET], settings[CCOLLAB])
        for panel in self.nbook.IterPages(True):
            panel.Configure(auth_changed, srv_changed)

    def __OnIconize(self, event):
        self.Hide()
        event.Skip()

    def __OnClose(self, event):
        if self.__IconizeOnClose__:
            self.Iconize()
            return

        def AppClose(size, iconized, position, active_page):
            with BusyInfo(self):
                self.settings.update({
                    FRAME_SIZE: size,
                    FRAME_ICON: iconized,
                    FRAME_POS: position,
                    ACTIVE_PAGE: '' if active_page is None else active_page.trunk.url,
                })
                self.trunks.save()
                TASK(self.GUILocker).stop(True)
                nbook = self.nbook
                [panel.StopMonitors() for panel in nbook.IterPages()]
                for thr in [panel.scheduler.stop() for panel in nbook.IterPages()]:
                    thr.join()
                self.scheduler.stop().join()
            wx.CallAfter(self.Destroy)

        self.EnableCloseButton(False)
        self.tray.RemoveIcon()
        self.tray.Destroy()
        Async(
            AppClose,
            self.GetSize(),
            self.IsIconized(),
            self.GetPosition(),
            self.nbook.GetCurPage()
        )
