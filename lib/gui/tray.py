import wx

from lib.common import Pluralizer
from lib.gui.__icon__ import AppIcon
from wx import NewId
from wx import PyAssertionError

SHOW_MENUITEM_ID = NewId()
EXIT_MENUITEM_ID = NewId()


class TrayMenu(wx.Menu):

    def __init__(self, tray):
        wx.Menu.__init__(self)
        self.tray = tray
        item = self.AddItem
        separator = self.AppendSeparator
        item(SHOW_MENUITEM_ID, 'Show/Hide window', self.__OnShowMenuItemSelection)
        separator()
        item(EXIT_MENUITEM_ID, 'Exit', self.__OnExitMenuItemSelection)
        tray.Bind(wx.EVT_TASKBAR_CLICK, self.__trayOnMenu)

    def AddItem(self, id, label, handler):
        self.AppendItem(wx.MenuItem(self, id, label))
        self.Bind(wx.EVT_MENU, handler, id=id)

    def __trayOnMenu(self, event):
        self.tray.PopupMenu(self)

    def __OnShowMenuItemSelection(self, event):
        self.tray.ToggleFrame()
        event.Skip()

    def __OnExitMenuItemSelection(self, event):
        frame = self.tray.frame
        frame.__IconizeOnClose__ = False
        frame.Close()
        event.Skip()


class TrayIcon(wx.TaskBarIcon):

    def __init__(self, frame):
        wx.TaskBarIcon.__init__(self)
        self.frame = frame
        self.__toggle = None
        self.menu = TrayMenu(self)
        self.SetIcon(AppIcon.GetIcon(), frame.Title)
        self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, self.__OnClick)
        self.Bind(wx.EVT_TASKBAR_BALLOON_CLICK, self.__OnBaloonClick)

    def DisplayBalloon(self, title=None, text=None, toggle_frame=None):
        self.__toggle = toggle_frame
        if title and text:
            self.ShowBalloon(title, text, flags=wx.ICON_INFORMATION)
            return
        count = 0
        diffs = []
        diffs_append = diffs.append
        for panel in self.frame.nbook.IterPages():
            i = len(panel.dlist)
            if i > 0:
                diffs_append((i, panel.trunk.url))
                count += i
        trunks_count = self.frame.nbook.GetPageCount()
        if count == 0:
            title = 'No diffs detected in {:N trunk/s}.'.format(Pluralizer(trunks_count))
            text = ' '
        else:
            title = '{:N diff/s} detected in {:N trunk/s}.'.format(
                Pluralizer(count), Pluralizer(trunks_count),
            )
            text = '\n'.join([
                '{:N diff/s} in {}'.format(Pluralizer(cnt), url)
                for cnt, url in sorted(diffs, reverse=True)
            ])[:256]
            if len(text) == 256:
                text = text[:text.rfind('\n')]
        try:
            self.ShowBalloon(title, text, flags=wx.ICON_INFORMATION)
        except PyAssertionError:
            pass

    def ToggleFrame(self):
        frame = self.frame
        iconized = frame.IsIconized()
        shown = frame.IsShown()
        frame.Iconize(not iconized)
        frame.Show(not shown)
        if frame.IsShown():
            frame.Raise()

    def __OnClick(self, event):
        self.DisplayBalloon()
        event.Skip()

    def __OnBaloonClick(self, event):
        if self.__toggle is None:
            self.ToggleFrame()
        else:
            self.__toggle()
        event.Skip()
