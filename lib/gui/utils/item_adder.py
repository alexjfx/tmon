from lib.gui.consts import GRAPHICS_DIR
from lib.gui.utils.thrsafe import threadsafe
from operator import itemgetter
from os.path import exists
from wx import AcceleratorEntry
from wx import AcceleratorTable
from wx import Bitmap
from wx import EVT_MENU
from wx import Menu
from wx import MenuItem
from wx import NullBitmap


class AccelTable(list):

    def add(self, *data):
        append = self.append
        for id, string in data:
            if '\t' not in string:
                continue
            accel = AcceleratorEntry(cmdID=id)
            accel.FromString(string)
            append(accel)

    def register(self, control):
        control.SetAcceleratorTable(AcceleratorTable(self))


class BaseItemAdder(object):

    @property
    def __items__(self):
        try:
            return self.__items
        except AttributeError:
            self.__items = {}
            return self.__items

    def __register(self, id, item_obj, visible=True):
        items = self.__items__
        items[id] = {
            'obj': item_obj,
            'idx': len(items),
            'visible': visible,
        }

    # not threadsafe because it is called mainly in __init__
    def AddItems(self, items):
        self.accel_table = AccelTable()
        add_accel = self.accel_table.add
        register = self.__register
        add = self.__AddItem__
        separator = self.__Separator__
        show_accels = getattr(self, 'show_accels', True)
        bind_obj = getattr(self, 'bind_obj', self.Parent)
        stub = lambda evt: evt.Skip()
        sep_id = 1
        for item in items:
            if item is None:
                register('Sep{}'.format(sep_id), separator())
                sep_id += 1
                continue

            if not isinstance(item, tuple) or len(item) < 3:
                raise ValueError('%r must be a tuple of at least three elements.' % item)

            id, string, handler = item[:3]
            if id is None:
                continue
            data = item[3] if len(item) > 3 else {}

            spl = string.split('\t')
            label = spl[0]
            accel = spl[1] if show_accels and len(spl) > 1 else ''
            bmp_path = GRAPHICS_DIR + '%s.png' % label.replace(' ', '_')
            bitmap = Bitmap(bmp_path) if exists(bmp_path) else NullBitmap

            register(id, add(id, label, accel, bitmap, data))
            add_accel((id, string))
            bind_obj.Bind(EVT_MENU, handler or stub, id=id)

        if show_accels:
            self.accel_table.register(bind_obj)

    @threadsafe
    def EnableItems(self, items):
        enable_item = self.__EnableItem__
        for id, enable in items.iteritems():
            enable_item(id, enable)

    @threadsafe
    def VisibleItems(self, items):
        reg_items = self.__items__
        add = self.__AddItemObj__
        separator = self.__Separator__

        self.Freeze()
        for id, visible in items.iteritems():
            reg_items[id]['visible'] = visible
        self.__ClearItems__()
        for item in sorted(reg_items.itervalues(), key=itemgetter('idx')):
            if not item['visible']:
                continue
            obj = item['obj']
            if obj is None:
                separator()
            else:
                add(obj)
        self.Thaw()

    def IsItemVisible(self, id):
        return self.__items__[id]['visible']

    def __Separator__(self):
        raise NotImplementedError

    def __AddItem__(self, id, label, accel, bitmap, data):
        raise NotImplementedError

    def __AddItemObj__(self, obj):
        raise NotImplementedError

    def __EnableItem__(self, id, enable):
        raise NotImplementedError

    def __ClearItems__(self):
        raise NotImplementedError


class ToolAdderMixin(BaseItemAdder):

    def AddItems(self, items):
        BaseItemAdder.AddItems(self, items)
        self.Realize()

    @threadsafe
    def VisibleItems(self, items):
        BaseItemAdder.VisibleItems(self, items)
        self.Realize()

    def __Separator__(self):
        self.AddSeparator()

    def __AddItem__(self, id, label, accel, bitmap, data):
        tl = label + (' (%s)' % accel if accel else '')
        tool = self.AddLabelTool(id, label, bitmap, bitmap.ConvertToDisabled(), shortHelp=tl)
        tool.Enable(data.get('enabled', False))
        return tool

    def __AddItemObj__(self, obj):
        self.AddToolItem(obj)

    def __EnableItem__(self, id, enable):
        self.EnableTool(id, enable)

    def __ClearItems__(self):
        remove = self.RemoveTool
        for id, item in self.__items__.iteritems():
            if item['obj'] is not None:
                remove(id)
        self.ClearTools()


class MenuItemAdderMixin(BaseItemAdder):

    def __Separator__(self):
        self.AppendSeparator()

    def __AddItem__(self, id, label, accel, bitmap, data):
        item = MenuItem(self, id, '\t'.join([label, accel]))
        item.SetBitmap(bitmap)
        submenu = data.get('submenu')
        if isinstance(submenu, Menu):
            item.SetSubMenu(submenu)
        self.AppendItem(item)
        return item

    def __EnableItem__(self, id, enable):
        self.Enable(id, enable)
