import wx

from lib.async import Async
from lib.gui.utils.inheritable import InheritableAccessMixin
from lib.gui.utils.item_adder import ToolAdderMixin
from lib.gui.utils.thrsafe import threadsafe
from wx import PyDeadObjectError


class BusyInfo(wx.Dialog):

    @threadsafe
    def __init__(self, parent, text='Please wait...', size=(200, 50), task=None):
        parent.Disable()
        wx.Dialog.__init__(self, parent, size=size, style=wx.FRAME_FLOAT_ON_PARENT)
        self.__task = task
        self.__noesc = True
        self.__text = wx.StaticText(self, label=text, style=wx.ALIGN_CENTRE)
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(self.__text, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.SetSizer(sz)
        self.Layout()
        self.Centre(wx.BOTH)
        self.Bind(wx.EVT_CHAR_HOOK, self.__OnKeyUp)
        self.Show()
        self.Update()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.Close()

    @threadsafe
    def ChangeText(self, text):
        self.__text.SetLabelText(text)
        self.__text.Center()

    @threadsafe
    def Close(self):
        self.Parent.Enable()
        self.Destroy()

    def __OnKeyUp(self, event):
        if event.GetKeyCode() == wx.WXK_ESCAPE and self.__noesc and self.__task is not None:
            self.__noesc = False
            self.ChangeText('Canceling...')

            def stop_task():
                self.__task.stop(True)
                try:
                    self.Close()
                except PyDeadObjectError:
                    pass

            Async(stop_task, name='AsyncTaskStop')
        event.Skip()


class CloseOnEscapeMixin:

    def __init__(self):
        self.Bind(wx.EVT_CHAR_HOOK, self.__OnKeyUp)

    def __OnKeyUp(self, event):
        if event.GetKeyCode() == wx.WXK_ESCAPE:
            self.Close()
        event.Skip()


def SaveableCombo(cmb, data_obj, data_attr, choices=[]):

    def parse(s):
        return tuple(sorted(i for i in {item.strip() for item in s.split(',')} if i))

    def parse_choices(c):
        return {i for i in (parse(items) for items in c) if i}

    def value(parsed):
        return ', '.join(parsed)

    def GetParsedValue():
        return value(parse(cmb.GetValue()))

    def UpdateChoices(choices=None):
        lst = [value(i) for i in parse_choices(data_obj.get(data_attr, []) + (choices or []))]
        cmb.Set(lst)
        data_obj[data_attr] = lst

    def Save():
        parsed = parse(cmb.GetValue())
        choices = cmb.GetStrings()
        if parsed and parsed not in parse_choices(choices):
            choices.append(value(parsed))
        data_obj[data_attr] = choices

    def __OnTextEnter(event):
        val = GetParsedValue()
        if val:
            UpdateChoices([val])
        cmb.SetValue(val)
        event.Skip()

    cmb.GetParsedValue = GetParsedValue
    cmb.UpdateChoices = UpdateChoices
    cmb.Save = Save
    cmb.Bind(wx.EVT_TEXT_ENTER, __OnTextEnter)

    UpdateChoices(choices)
    return cmb


class BaseToolBar(wx.ToolBar, ToolAdderMixin, InheritableAccessMixin):

    def __init__(self, parent, tool_buttons=None):
        wx.ToolBar.__init__(self, parent, style=wx.TB_FLAT | wx.TB_HORIZONTAL)
        if tool_buttons is not None:
            self.AddItems(tool_buttons)


@threadsafe
def BringToFront(window):
    window.Iconize(False)
    window.Maximize(False)
    window.Show()
    window.Raise()


@threadsafe
def MessageDlg(parent, msg, caption, style):
    with wx.MessageDialog(parent, msg, caption, style) as dlg:
        return dlg.ShowModal()


ErrorDlg = lambda parent, msg: MessageDlg(parent, msg, 'Error', wx.OK | wx.ICON_ERROR)
InfoDlg = lambda parent, msg: MessageDlg(parent, msg, 'Info', wx.OK | wx.ICON_INFORMATION)


def lock_errdlg(message=''):
    def wrapper(self, *args, **kwargs):
        sep = ' ' if message else ''
        # up to this moment, the lock may be released
        # so, the info message should be correctly handled
        owner = kwargs['lock'].owner
        task = '<%s>' % owner if owner else 'A task'
        ErrorDlg(self, sep.join([
            message, '\n\n%s is currently locking the trunk.\nPlease try again later.' % task
        ]))
    return wrapper
