class Inheritable(object):

    def __init__(self, name):
        self.__name = name

    def __get__(self, instance, cls):
        if instance is None:
            return self
        return getattr(instance, self.__name, None)

    def __set__(self, instance, val):
        setattr(instance, self.__name, val)

    def __delete__(self, instance):
        delattr(instance, self.__name)


class InheritableAccessMixin:

    def __getattr__(self, name):
        if name[0] == '_':
            raise AttributeError('%r is not found in %r.' % (name, self))
        obj = self
        n = '__%s__' % name
        while True:
            obj = obj.Parent
            try:
                val = obj.__dict__[n]
            except KeyError:
                continue
            except AttributeError:
                raise AttributeError('%r is not found in %r.' % (name, self))
            if isinstance(getattr(type(obj), name, None), Inheritable):
                return val
            else:
                raise AttributeError('%r is found in %r but it is not inheritable.' % (name, obj))


def inheritables(*names):
    def wrapper(cls):
        for name in names:
            setattr(cls, name, Inheritable('__%s__' % name))
        return cls
    return wrapper
