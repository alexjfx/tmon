from functools import wraps
from lib.async.task import istask
from logging import exception
from types import FunctionType
from types import MethodType
from wx import CallAfter
from wx import PyDeadObjectError
from wx import Thread_IsMain

__THRSAFE = '__threadsafe__'
isthrsafe = lambda func: getattr(func, __THRSAFE, False)


class __EmptyResult:
    pass


def threadsafe(func):
    RESULT = '__result__'
    empty = __EmptyResult()

    def call(func):
        def wrapper(*args, **kwargs):
            res = kwargs.pop(RESULT)
            try:
                res[0] = func(*args, **kwargs)
            except PyDeadObjectError:
                # exception('Cannot access deleted %r.' % func)
                res[0] = None
            except Exception as e:
                exception(e)
                res[0] = None
        return wrapper

    @wraps(func)
    def wrapper(*args, **kwargs):
        if Thread_IsMain():
            return func(*args, **kwargs)

        kwargs[RESULT] = res = [empty]
        CallAfter(call(func), *args, **kwargs)
        while res[0] is empty:
            pass
        return res[0]
    setattr(wrapper, __THRSAFE, True)

    return wrapper


class ThreadSafe(type):

    def __call__(self, *args, **kwargs):
        obj = type.__call__(self, *args, **kwargs)

        for attrname in (a for a in dir(obj) if a[0] != '_'):
            try:
                attr = getattr(obj, attrname)
            except Exception:
                exception('Cannot make %r threadsafe in %r.' % (attrname, obj))
                continue
            if isinstance(attr, (MethodType, FunctionType)) and not istask(attr):
                setattr(obj, attrname, threadsafe(attr))

        return obj


class ThreadSafeCtrl:
    __metaclass__ = ThreadSafe
