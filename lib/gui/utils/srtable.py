class SortedTable:

    def __init__(self, sorted_col):
        self.__srtcol = sorted_col
        self.__items = []

    def __iter__(self):
        for x in enumerate(self.__items[:]):
            yield x

    def __repr__(self):
        contents = '\n'.join(['{}: {}'.format(i, x) for i, x in self])
        return '{name}\n{sep}\n{contents}\n{sep}'.format(
            name=self.__class__.__name__,
            sep='=' * 80,
            contents=contents if contents else 'Empty table',
        )

    def __len__(self):
        return len(self.__items)

    def __nonzero__(self):
        return True

    def __getitem__(self, value):
        if isinstance(value, int):
            try:
                return self.__items[value]
            except IndexError:
                return None
        col, st = value
        try:
            return next(x for x in self if x[1][col] == st)
        except StopIteration:
            return None, None

    def __delitem__(self, index):
        try:
            del self.__items[index]
        except IndexError:
            pass

    def add(self, item):
        items = self.__items
        col = self.__srtcol
        x = (item[col],) + item
        index = 0
        hi = len(items)
        while index < hi:
            mid = (index + hi) // 2
            mid_item = items[mid]
            y = (mid_item[col],) + mid_item
            if x > y:
                hi = mid
            else:
                index = mid + 1
        items.insert(index, item)
        return index


if __name__ == '__main__':
    from random import randint

    tbl = SortedTable(0)
    print bool(tbl)
    tbl.add((42, 'default'))
    for i in range(1, 11):
        tbl.add((randint(-100, 100), 'item%d' % i))
        tbl.add((11, 'item%d' % i))
    print bool(tbl)
    print tbl
    print len(tbl)
    print tbl[5]
    print tbl[1, 'item10']
    print tbl[1, 'item11']
    print tbl[0, 42]
    print tbl[0, 11]
