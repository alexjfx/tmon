from lib.common import APP_DIR

CONFIG_DIR = APP_DIR + '/config/'
GRAPHICS_DIR = APP_DIR + '/graphics/'
THIRDPARTY_DIR = APP_DIR + '/third-party/'

TRUNKS_DB = CONFIG_DIR + 'trunks.pkl'
SETTINGS_DB = CONFIG_DIR + 'settings.pkl'

# settings
CREDS = 'credentials'
CCOLLAB = 'ccollab'
BITBUCKET = 'bitbucket'
TMON = 'trmon_interval'
RMON = 'revmon_interval'
DIFF_TOOL = 'diff_tool'
EDIT_TOOL = 'edit_tool'
TERM_TOOL = 'term_tool'
FRAME_SIZE = 'frame_size'
FRAME_ICON = 'frame_icon'
FRAME_POS = 'frame_pos'
ACTIVE_PAGE = 'active_page'
SVN_BLAME = 'svn', 'blame'
GIT_BLAME = 'git', 'blame'
SVN_LOG = 'svn', 'log'
GIT_LOG = 'git', 'log'

# auto-complete attrs
URLS = 'urls'
CHECK_FILES = 'check_files'
IGNORE_DIRS = 'ignore_dirs'
REVIEWERS = 'reviewers'
TASK_IDS = 'task_ids'
REVIEW_TITLES = 'review_titles'

# states
MODIFIED = -1
NEW_DIFF = -2
UNTRACKED = -3
NO_REVIEW = {NEW_DIFF, MODIFIED, UNTRACKED, None}

# highlight color
LIGHT_GREEN = 170, 209, 183
