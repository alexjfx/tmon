import wx

from filecmp import cmp
from functools import partial
from lib.async import Async
from lib.async.lock import get_lock
from lib.async.task import task
from lib.cmdline import cmd
from lib.common import APP_DIR
from lib.common import delete_file
from lib.common import hexhash
from lib.common import match_file_pattern
from lib.gui.consts import *
from lib.gui.diffctrls.identifiers import *
from lib.gui.diffctrls.popupmenu import DiffPopupMenu
from lib.gui.utils.inheritable import InheritableAccessMixin
from lib.gui.utils.item_adder import AccelTable
from lib.gui.utils.srtable import SortedTable
from lib.gui.utils.thrsafe import threadsafe
from logging import error
from logging import info
from logging import warning
from os.path import basename
from os.path import exists
from os.path import isfile
from threading import Lock
from wx import ListCtrl
from wx import NOT_FOUND
from wx import PyDeadObjectError
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin


def __alias__(state):
    if state == NEW_DIFF:
        return 'New'
    if state == MODIFIED:
        return 'Modified'
    if state == UNTRACKED:
        return 'Untracked'
    if isinstance(state, int):
        return '#%d' % state
    return 'Unknown'


class DiffListCtrl(ListCtrl, ListCtrlAutoWidthMixin, SortedTable, InheritableAccessMixin):

    def __init__(self, parent):
        ListCtrl.__init__(self, parent, style=wx.LC_VRULES | wx.LC_REPORT | wx.LC_NO_SORT_HEADER)
        self.InsertColumn(STATE_COL, 'State')
        self.InsertColumn(PATH_COL, 'Path')

        self.lock = Lock()
        self.__bgclr = clr = wx.SystemSettings.GetColour(wx.SYS_COLOUR_INFOBK)
        self.SetBackgroundColour(clr)

        self.__col_count = self.GetColumnCount()
        self.menu = DiffPopupMenu(self)
        ListCtrlAutoWidthMixin.__init__(self)
        SortedTable.__init__(self, STATE_COL)

        accel_table = AccelTable()
        accel_table.add(
            (SELECTALL_SHORTCUT, '\tCtrl+A'),
            (DESELECTALL_SHORTCUT, '\tEsc'),
            (DEBUG_SHORTCUT, '\tCtrl+Shift+D'),
        )
        accel_table.extend(self.menu.accel_table)
        accel_table.register(self)

        set_evt = self.Bind
        set_evt(wx.EVT_LIST_INSERT_ITEM, self.__OnChangeItem)
        set_evt(wx.EVT_LIST_DELETE_ITEM, self.__OnChangeItem)
        set_evt(wx.EVT_LIST_DELETE_ALL_ITEMS, self.__OnChangeItem)
        set_evt(wx.EVT_LIST_ITEM_ACTIVATED, self.__OnItemActivated)
        set_evt(wx.EVT_MENU, self.__OnSelectAllItems, id=SELECTALL_SHORTCUT)
        set_evt(wx.EVT_MENU, self.__OnSelectAllItems, id=DESELECTALL_SHORTCUT)
        set_evt(wx.EVT_MENU, self.__OnDebug, id=DEBUG_SHORTCUT)

    # ======================= #
    # DiffListCtrl item funcs #
    # ======================= #

    def CreateItemObj(self, path, state=NEW_DIFF):
        # make configurable list based on state and path indexes
        item = [None] * self.__col_count
        item[STATE_COL] = state
        item[PATH_COL] = path

        return tuple(item)

    @property
    @threadsafe
    def SelectedItems(self):
        issel = self.IsSelected
        return [item for index, item in self if issel(index)]

    @property
    @threadsafe
    def SelectedItem(self):
        index = self.GetNextItem(NOT_FOUND, wx.LIST_NEXT_ALL, wx.LIST_STATE_SELECTED)
        return self[index] if index != NOT_FOUND else None

    @threadsafe
    def AddItem(self, path, state=NEW_DIFF):
        index = self.InsertStringItem(self.add(self.CreateItemObj(path, state)), '')
        set_str = self.SetStringItem
        set_str(index, STATE_COL, __alias__(state))
        set_str(index, PATH_COL, self.trunk.shorten(path))

        return index

    @threadsafe
    def ChangeState(self, index, new_state):
        item = self[index]
        if item[STATE_COL] != new_state:
            self.RemoveItem(index)
            index = self.AddItem(item[PATH_COL], new_state)
        return index

    @threadsafe
    def RemoveItem(self, index):
        if index is None:
            return
        try:
            del self[index]
            self.DeleteItem(index)
        except IndexError as e:
            error('Cannot remove item with index %d: %s' % (index, e))

    @threadsafe
    def Clear(self):
        self.items = []
        self.DeleteAllItems()

    @threadsafe
    def SetItemColor(self, index, color):
        self.SetItemBackgroundColour(index, color)

    @task(skip_if_found=lambda _, path, **kwargs: path)
    @get_lock()
    def ProcessPath(self, path, deleted=False, modified=None):
        # get index by path
        index, _ = self[PATH_COL, path]

        # file deleted event or file not found
        if deleted or not isfile(path):
            info('Deleting: {path} ({self.trunk.url})'.format(**locals()))
            self.RemoveItem(index)
            rev_id = self.rmon.review_id(path)
            if rev_id is not None:
                warning(
                    'Deleted file "{path}" ({self.trunk.url}) is on review #{rev_id}. '
                    'This review will no longer be monitored.'.format(**locals())
                )
                Async(self.rmon.unwatch_review, rev_id, name='UnwatchReview')
            return

        # collect all required stat
        trunk = self.trunk
        info('Processing: {path} ({trunk.url})'.format(**locals()))
        try:
            same_files = cmp(trunk.irev_pth(path), path)
        except (OSError, IOError) as e:
            error('Cannot compare "{}": {}'.format(path, e))
            return
        mod = trunk.vcs.MODIFIED(path) if modified is None else modified
        rev_id = self.rmon.review_id(path)
        dont_check = not match_file_pattern(path, trunk.check_files)
        info('\n\t'.join([
            'Process state for "{path}" ({trunk.url}):',
            'Index in DiffListCtrl: {index}',
            'Same as file from init_rev: {same_files}',
            'Modified in VCS: {mod}',
            'Review #: {rev_id}',
            'Do not check: {dont_check}',
        ]).format(**locals()))

        # define if item is done and delete if added
        if (same_files or dont_check) and not mod and rev_id is None:
            self.RemoveItem(index)
            return

        # update review file
        if rev_id is not None:
            Async(self.rmon.update_review, path, rev_id, name='UpdateReview')

        if index is None:
            itemfn = partial(self.AddItem, path)
        else:
            itemfn = partial(self.ChangeState, index)

        if rev_id is None:
            index = itemfn(MODIFIED if mod else NEW_DIFF)
        else:
            index = itemfn(rev_id)

        # set color for identical files
        self.SetItemColor(index, LIGHT_GREEN if same_files else self.__bgclr)

    # =================== #
    # DiffListCtrl events #
    # =================== #

    def __OnChangeItem(self, event):
        i = len(self)
        self.nbook.UpdatePageText(self.panel, i)
        self.stbar[DIFFS_FIELD] = i
        if event.GetEventType() == wx.wxEVT_COMMAND_LIST_INSERT_ITEM:
            try:
                self.tray.DisplayBalloon()
            except PyDeadObjectError:
                pass
        event.Skip()

    def __diff__(self, right, left, with_vcs, diff_tool):
        # determine left path
        if left is None:
            if not with_vcs:
                left = self.trunk.irev_pth(right)
            else:
                left = APP_DIR + '/%s_%s' % (hexhash(right), basename(right))
                if not self.trunk.vcs.GET_FILE_LASTREV(right, left):
                    return

        if not exists(right):
            error('File not found: %s' % right)
            return
        if not exists(left):
            error('File not found: %s' % left)
            return

        # do diff
        if not cmd(' '.join([diff_tool, left, right]), hide_wnd=False):
            return
        try:
            self.ProcessPath(right)
        except PyDeadObjectError:
            pass

        # delete tempfile in case of vcs comparison
        if with_vcs:
            delete_file(left)

    def DiffItem(self, right, left=None, with_vcs=None):
        diff_tool = self.settings[DIFF_TOOL]
        if not right or not diff_tool:
            return
        if not exists(diff_tool):
            error('Diff tool not found: %s' % diff_tool)
            return
        if left is None and with_vcs is None:
            with_vcs = not self.trunk.check_files
        Async(self.__diff__, right, left, with_vcs, diff_tool, name='DiffItem')

    def __OnItemActivated(self, event):
        self.DiffItem(self[event.Index][PATH_COL])
        event.Skip()

    def __OnSelectAllItems(self, event):
        index = NOT_FOUND
        select = partial(
            self.SetItemState,
            state=wx.LIST_STATE_SELECTED if event.Id == SELECTALL_SHORTCUT else 0,
            stateMask=wx.LIST_STATE_SELECTED
        )
        next_item = partial(self.GetNextItem)
        while True:
            index = next_item(index)
            if index == NOT_FOUND:
                break
            select(index)
        event.Skip()

    def __OnDebug(self, event):
        info('\n'.join([
            SortedTable.__repr__(self),
            repr(self.trunk),
        ]))
        event.Skip()
