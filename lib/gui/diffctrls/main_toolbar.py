import wx

from lib.async.lock import get_lock
from lib.async.task import TASK
from lib.async.task import task
from lib.gui.__frames__ import TrunkDeleteDialog
from lib.gui.consts import *
from lib.gui.dialogs.search.dialog import SearchDialog
from lib.gui.dialogs.trunk_edit_dialog import TrunkEditDialog
from lib.gui.diffctrls.identifiers import *
from lib.gui.utils.common import *
from lib.gui.utils.thrsafe import threadsafe
from lib.rest.bitbucket import BitbucketError
from lib.rest.bitbucket import BitbucketSession
from lib.trunk.trunk import Trunk


class MainToolBar(BaseToolBar):

    def __init__(self, parent):
        BaseToolBar.__init__(self, parent, [
            (SETTINGS_TOOL_ID, 'Settings\tF12', parent.ConfigureSettings, {'enabled': True}),
            None,
            (ADDTRUNK_TOOL_ID, 'Add trunk\tIns', self.__OnAddTrunkToolClicked, {'enabled': True}),
            (DELTRUNK_TOOL_ID, 'Remove trunk\tDel', self.__OnDelTrunkToolClicked),
            (SEARCH_TOOL_ID, 'Search\tF3', self.__OnSearchToolClicked),
            (LOG_TOOL_ID, 'Log window\tCtrl+L', self.__OnLogToolClicked, {'enabled': True}),
            None,
            (EXIT_TOOL_ID, 'Exit', self.__OnExitToolClicked, {'enabled': True}),
        ])

    # ================== #
    # MainToolBar events #
    # ================== #

    @task
    @get_lock()
    def AddTrunk(self, trunk, lrep):
        if not self.trunks.add(trunk):
            return
        nbook = self.nbook
        panel = nbook.CreatePage(trunk, True)
        panel.stbar[MONITOR_FIELD] = 'Checking out files...'
        nbook.UpdatePageText(panel, text='ADDING:')
        trunk.set_local_rep(lrep)
        nbook.UpdatePageText(panel)
        # Stopped GUILocker means that app is stopping and no need to call StartMonitors
        if not TASK(self.main_frame.GUILocker).is_stopped():
            self.tray.DisplayBalloon('Downloaded trunk.', trunk.url)
            panel.StartMonitors()

    def __fork(self, url):
        gls = self.settings
        with BitbucketSession(gls[BITBUCKET], gls[CREDS], url) as session:
            repository = session.repository
            if (
                not repository['valid'] or
                repository['key'] != repository['origin_key'] or
                MessageDlg(
                    self.main_frame,
                    'Do you want to fork this trunk to your account?\n\n'
                    'The forked version will be monitored and automatically synced with '
                    'the original version.\n'
                    'This is a preferred way to work with Git trunks.',
                    'Fork Trunk',
                    wx.YES | wx.NO | wx.ICON_INFORMATION
                ) == wx.ID_NO
            ):
                return url
            with BusyInfo(self.main_frame, 'Forking trunk...'):
                try:
                    return session.fork_repository()
                except BitbucketError as e:
                    ErrorDlg(self.main_frame, str(e))

    @task
    def CreateTrunk(self, lrep=None, vcs=None, **settings):
        if vcs == 'Git':
            url = self.__fork(settings['url'])
            if url is None:
                return
            settings['url'] = url
        trunk = Trunk(**settings)
        self.AddTrunk(trunk, lrep, lock=trunk.lock)

    def __OnAddTrunkToolClicked(self, event):
        with TrunkEditDialog(self.main_frame, 'Add Trunk') as dlg:
            dlg.LocalRepPicker.SetPath('C:\\trunks')
            dlg.IgnoreDirsCombo.SetValue('.git, .svn')
            dlg.sdbSizerOK.Enable(False)
            if dlg.ShowModal() == wx.ID_OK:
                self.CreateTrunk(
                    url=dlg.UrlCombo.GetParsedValue(),
                    check_files=dlg.CheckFilesCombo.GetParsedValue(),
                    ignore_dirs=dlg.IgnoreDirsCombo.GetParsedValue(),
                    reviewer=dlg.ReviewerCombo.GetParsedValue(),
                    auto_diff=dlg.AutoDiffCbx.GetValue(),
                    lrep=dlg.LocalRepPicker.GetPath(),
                    vcs=dlg.VcsValue.GetLabel(),
                )

    @task
    @get_lock(False, lock_errdlg('Cannot delete the trunk.'))
    def DeleteTrunk(self, panel, delete_files=False):
        threadsafe(panel.Disable)()
        panel.StopMonitors()
        panel.scheduler.stop().join()
        panel.dlist.Clear()
        trunk = panel.trunk
        if delete_files:
            panel.stbar[MONITOR_FIELD] = 'Deleting files...'
            self.nbook.UpdatePageText(panel, text='DELETING:')
            trunk.set_local_rep()
        self.nbook.DestroyPage(panel)
        del self.trunks[trunk]
        self.tray.DisplayBalloon(
            'Deleted trunk.' if delete_files else 'Removed trunk.', trunk.url
        )

    def __OnDelTrunkToolClicked(self, event):
        panel = self.nbook.GetCurPage()
        if not panel:
            return
        with TrunkDeleteDialog(self.main_frame) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                self.DeleteTrunk(panel, dlg.DelCbx.IsChecked(), lock=panel.trunk.lock)

    def __OnSearchToolClicked(self, event):
        panel = self.nbook.GetCurPage()
        if panel:
            SearchDialog(panel).Show()

    def __OnLogToolClicked(self, event):
        log = self.log
        log.Show(not log.IsShown())

    def __OnExitToolClicked(self, event):
        frame = self.main_frame
        frame.__IconizeOnClose__ = False
        frame.Close()
