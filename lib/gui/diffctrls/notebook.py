import wx

from lib.gui.diffctrls.identifiers import *
from lib.gui.diffctrls.panel import DiffPanel
from lib.gui.utils.inheritable import InheritableAccessMixin
from lib.gui.utils.thrsafe import threadsafe
from wx import PyDeadObjectError

LOCAL_MARK = '[local changes only]'


def __nbook_srtkey__(st):
    lc = LOCAL_MARK in st
    i = 0
    if st[0] == '(':
        i, st = st[1:].split(') ', 1)
        i = i.strip('*')
    return -int(i), lc, st


class DiffNoteBook(wx.Choicebook, InheritableAccessMixin):

    def __init__(self, parent):
        wx.Choicebook.__init__(self, parent, wx.NewId())
        self.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGED, self.__OnPageChanged)

    def __UpdateToolBar(self):
        pages = bool(self.GetPageCount())
        enable = self.main_tlbar.EnableTool
        enable(DELTRUNK_TOOL_ID, pages)
        enable(SEARCH_TOOL_ID, pages)

    def __GetPageIndex(self, panel):
        trunk = panel.trunk
        get_page = self.GetPage
        for i in xrange(self.GetPageCount()):
            if get_page(i).trunk == trunk:
                return i

    @threadsafe
    def UpdatePageText(self, panel, diff_count=None, text=''):
        if diff_count is None:
            diff_count = len(panel.dlist)
        trunk = panel.trunk

        reviews = '*' if panel.rmon.reviews else ''
        label = '{text}{diffs}{url} [{hash}]{local}'.format(
            text=text + ' ' if text else '',
            diffs='({}{}) '.format(diff_count, reviews) if diff_count else '',
            url=trunk.url,
            hash=trunk.md5hash,
            local=LOCAL_MARK if not trunk.check_files else '',
        )

        cur_idx = self.__GetPageIndex(panel)
        cur_lbl = self.GetPageText(cur_idx)
        if cur_lbl == label:
            return

        labels = sorted(
            (lbl for lbl in self.GetChoiceCtrl().GetStrings() + [label] if lbl != cur_lbl),
            key=__nbook_srtkey__,
        )

        new_idx = labels.index(label)
        if cur_idx == new_idx:
            self.SetPageText(cur_idx, label)
            return

        sel_idx = self.GetSelection()
        self.RemovePage(cur_idx)
        self.InsertPage(new_idx, panel, label, cur_idx == sel_idx)

    @threadsafe
    def CreatePage(self, trunk, select=False):
        panel = DiffPanel(self, trunk)
        self.InsertPage(0, panel, '', select)
        self.UpdatePageText(panel)
        self.__UpdateToolBar()
        return panel

    @threadsafe
    def GetCurPage(self):
        return self.GetCurrentPage()

    @threadsafe
    def IterPages(self, start_from_current=False):
        get_page = self.GetPage
        if start_from_current:
            cur_page = self.GetCurrentPage()
            yield cur_page
        for i in xrange(self.GetPageCount()):
            try:
                page = get_page(i)
                if start_from_current and page == cur_page:
                    continue
                yield page
            except PyDeadObjectError:
                pass

    @threadsafe
    def DestroyPage(self, panel):
        self.DeletePage(self.__GetPageIndex(panel))
        self.__UpdateToolBar()

    def __OnPageChanged(self, event):
        self.GetCurrentPage().dlist.SetFocus()
        event.Skip()
