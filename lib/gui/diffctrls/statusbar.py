from lib.gui.utils.thrsafe import threadsafe
from wx import StatusBar


class DefaultStatusValue:
    pass


class DiffStatusBar(StatusBar):

    def __init__(self, parent, *columns):
        StatusBar.__init__(self, parent)
        self.__callbacks = {}
        self.SetFieldsCount(len(columns))
        self.SetStatusWidths([w[0] if isinstance(w, (tuple, list)) else w for w in columns])
        for i, dt in enumerate(columns):
            if isinstance(dt, (tuple, list)):
                self.SetStatusText(dt[1], i)

    def callback(self, field, func, default=DefaultStatusValue()):
        self.__callbacks[field] = func
        if not isinstance(default, DefaultStatusValue):
            self.__setitem__(field, default)

    @threadsafe
    def __setitem__(self, field, value):
        func = self.__callbacks.get(field)
        self.SetStatusText(value if func is None else func(value), field)
