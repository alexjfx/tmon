import wx

from lib.async import Async
from lib.cmdline import cmd
from lib.common import join_paths
from lib.gui.consts import *
from lib.gui.diffctrls.identifiers import *
from lib.gui.utils.common import ErrorDlg
from lib.gui.utils.item_adder import MenuItemAdderMixin
from logging import error
from logging import info
from os.path import exists
from webbrowser import open_new_tab
from wx import Menu
from wx import SingleChoiceDialog
from wx import TextDataObject
from wx import TheClipboard


def copy2clipboard(text):
    if not TheClipboard.Open():
        error('Cannot open the clipboard.')
        return

    TheClipboard.SetData(TextDataObject(text))
    TheClipboard.Flush()
    TheClipboard.Close()
    info('Copied to the clipboard:\n' + text)


class UrlListMenu(Menu):

    def __init__(self, parent):
        Menu.__init__(self)
        self.SetParent(parent)
        self.__data = {}

    def UpdateItems(self, selected_item):
        for item in self.GetMenuItems():
            self.DeleteItem(item)
        self.__data.clear()
        parent = self.Parent
        if selected_item is None or not parent.dlist.settings[DIFF_TOOL]:
            return
        data = self.__data
        trunk = parent.dlist.trunk
        url = trunk.url
        selected_path = trunk.shorten(selected_item[PATH_COL])
        for tr in parent.dlist.trunks:
            trunk_url = tr.url
            if trunk_url == url:
                continue
            trunk_path = tr.last_rev
            if trunk_path is None:
                continue
            if not exists(join_paths(trunk_path, selected_path)):
                continue
            id = wx.NewId()
            parent.Bind(wx.EVT_MENU, self.__OnMenuItemSelection, self.Append(id, trunk_url))
            data[id] = trunk_url, trunk_path

    def __OnMenuItemSelection(self, event):
        dlist = self.Parent.dlist
        if not dlist.settings[DIFF_TOOL]:
            return
        _, pth = self.__data[event.Id]
        shorten = dlist.trunk.shorten
        diff = dlist.DiffItem
        errors = []
        for item in dlist.SelectedItems:
            left = join_paths(pth, shorten(item[PATH_COL]))
            if exists(left):
                right = item[PATH_COL]
                diff(right, left)
            else:
                errors.append(left)
        if errors:
            ErrorDlg(dlist, 'Cannot diff against unexisting files\n\n' + '\n'.join(errors))


class DiffPopupMenu(Menu, MenuItemAdderMixin):

    diff_paths = {}  # id -> path alias, full path

    def __init__(self, dlist, show_accels=True):
        Menu.__init__(self)
        self.dlist = dlist
        self.__urllist_menu = UrlListMenu(self)

        self.bind_obj = dlist
        self.show_accels = show_accels
        self.AddItems([
            (DIFF_MENUITEM_ID, 'View diff\tEnter', self.__OnDiffMenuItemSelection),
            (VCSDIFF_MENUITEM_ID, 'View VCS diff\tCtrl+Enter', self.__OnDiffMenuItemSelection),
            (DIFFWITH_MENUITEM_ID, 'Diff against', None, {'submenu': self.__urllist_menu}),
            (DIFF_SOURCE_ID, 'Diff source:', self.__OnCustomDiffMenuItemSelection),
            (DIFF_TARGET_ID, 'Diff target:', self.__OnCustomDiffMenuItemSelection),
            None,
            (BLAME_MENUITEM_ID, 'Blame\tF9', self.__OnBlameMenuItemSelection),
            (LOG_MENUITEM_ID, 'Show log\tF10', self.__OnLogMenuItemSelection),
            None,
            (SHELL_MENUITEM_ID, 'Open in shell\tAlt+Enter', self.__OnShellMenuItemSelection),
            (EDITOR_MENUITEM_ID, 'Open in editor\tF1', self.__OnEditorMenuItemSelection),
            (TERMINAL_MENUITEM_ID, 'Open terminal\tF8', self.__OnTerminalMenuItemSelection),
            None,
            (BROWSE_MENUITEM_ID, 'Browse review', self.__OnBrowseMenuItemSelection),
            (REVURL_MENUITEM_ID, 'Copy review URL', self.__OnReviewUrlMenuItemSelection),
            (UNWATCH_MENUITEM_ID, 'Unwatch review', self.__OnUnwatchMenuItemSelection),
            None,
            (URL_MENUITEM_ID, 'Copy URL\tAlt+C', self.__OnUrlMenuItemSelection),
            (PATH_MENUITEM_ID, 'Copy path\tCtrl+C', self.__OnPathMenuItemSelection),
            (HASH_MENUITEM_ID, 'Copy trunk hash\tShift+C', self.__OnHashMenuItemSelection),
        ])

        dlist.Bind(wx.EVT_RIGHT_DOWN, self.__dlistOnContextMenu)

    def __set_diff_label(self, id):
        header = self.GetLabel(id).split(':')[0]
        path_alias, _ = self.diff_paths.get(id, ('Select', None))
        self.SetLabel(id, '%s: %s' % (header, path_alias))

    def __dlistOnContextMenu(self, event):
        dlist = self.dlist
        item = dlist.SelectedItem
        isitem = bool(item)
        isreview = isitem and item[STATE_COL] not in NO_REVIEW
        files_to_check = bool(dlist.trunk.check_files)
        vcs_detected = dlist.trunk.vcs.name != 'unknown'
        diff_tool = bool(dlist.settings[DIFF_TOOL])

        self.EnableItems({
            DIFF_MENUITEM_ID: diff_tool and isitem and files_to_check,
            VCSDIFF_MENUITEM_ID: diff_tool and isitem and (
                not files_to_check or item[STATE_COL] != NEW_DIFF
            ),
            BLAME_MENUITEM_ID: isitem and vcs_detected,
            LOG_MENUITEM_ID: vcs_detected,
            EDITOR_MENUITEM_ID: bool(dlist.settings[EDIT_TOOL]) and isitem,
            BROWSE_MENUITEM_ID: isreview,
            REVURL_MENUITEM_ID: isreview,
            UNWATCH_MENUITEM_ID: isreview,
        })

        self.__urllist_menu.UpdateItems(item)
        self.__set_diff_label(DIFF_SOURCE_ID)
        self.__set_diff_label(DIFF_TARGET_ID)

        dlist.PopupMenu(self, event.GetPosition())

    def __OnDiffMenuItemSelection(self, event):
        with_vcs = event.Id == VCSDIFF_MENUITEM_ID
        dlist = self.dlist
        files_to_check = bool(dlist.trunk.check_files)
        selitems = dlist.SelectedItems
        diff = dlist.DiffItem
        if with_vcs:
            paths = (
                item[PATH_COL]
                for item in selitems
                if (not files_to_check or item[STATE_COL] != NEW_DIFF)
            )
        else:
            paths = (item[PATH_COL] for item in selitems)
        [diff(pth, with_vcs=with_vcs) for pth in paths]

    def __OnBrowseMenuItemSelection(self, event):
        rev_id = self.dlist.SelectedItem[STATE_COL]
        open_new_tab(self.dlist.rmon.review_url(rev_id))

    def __OnShellMenuItemSelection(self, event):
        item = self.dlist.SelectedItem
        if item is not None:
            cmd('explorer /select,{pth}', pth=item[PATH_COL], hide_wnd=False)
        else:
            cmd('explorer {pth}', pth=self.dlist.trunk.last_rev, hide_wnd=False)

    def __OnEditorMenuItemSelection(self, event):
        ed = self.dlist.settings[EDIT_TOOL]
        if not exists(ed):
            ed = 'notepad'
        for item in self.dlist.SelectedItems:
            Async(cmd, '{ed} {pth}', ed=ed, pth=item[PATH_COL], hide_wnd=False, name='Editor')

    def __OnTerminalMenuItemSelection(self, event):
        self.dlist.tlbar._OnTerminalToolClicked(event, self.dlist.SelectedItem)

    def __OnUrlMenuItemSelection(self, event):
        tr = self.dlist.trunk
        URL = tr.vcs.URL
        url_gen = (URL(item[PATH_COL]) for item in self.dlist.SelectedItems)
        urls = [url for url in url_gen if url is not None]
        copy2clipboard('\n'.join(urls) if urls else tr.url)

    def __OnReviewUrlMenuItemSelection(self, event):
        url = self.dlist.rmon.review_url(self.dlist.SelectedItem[STATE_COL])
        if url:
            copy2clipboard(url)

    def __OnUnwatchMenuItemSelection(self, event):
        dlist = self.dlist
        Async(dlist.rmon.unwatch_review, dlist.SelectedItem[STATE_COL], name='UnwatchReview')

    def __OnPathMenuItemSelection(self, event):
        paths = [item[PATH_COL] for item in self.dlist.SelectedItems]
        copy2clipboard('\n'.join(paths) if paths else self.dlist.trunk.last_rev)

    def __OnHashMenuItemSelection(self, event):
        copy2clipboard(self.dlist.trunk.md5hash)

    def __diff_check(self, id):
        if id != DIFF_TARGET_ID:
            return
        _, left = self.diff_paths.get(DIFF_SOURCE_ID, (None, None))
        _, right = self.diff_paths.get(DIFF_TARGET_ID, (None, None))
        if left is not None and right is not None and left != right:
            self.dlist.DiffItem(right, left)

    def __OnCustomDiffMenuItemSelection(self, event):
        item = self.dlist.SelectedItem
        id = event.GetId()
        if item is None:
            return self.__diff_check(id)
        tr = self.dlist.trunk
        pth = item[PATH_COL]
        self.diff_paths[id] = '[%s] %s' % (tr.url, tr.shorten(pth)), pth
        self.__diff_check(id)

    def __OnBlameMenuItemSelection(self, event):
        item = self.dlist.SelectedItem
        if item is None:
            return
        vcs = self.dlist.trunk.vcs
        cmd_str = self.dlist.settings[vcs.name.lower(), 'blame']
        Async(vcs.COMMAND, cmd_str, item[PATH_COL], name='BlameWindow')

    def __show_log(self, pth, diff_after=False):
        dlist = self.dlist
        vcs = dlist.trunk.vcs
        vcs.COMMAND(dlist.settings[vcs.name.lower(), 'log'], pth)
        if diff_after:
            dlist.tlbar.DiffTask.start()

    def __OnLogMenuItemSelection(self, event):
        dlist = self.dlist
        with SingleChoiceDialog(
            dlist,
            'Select the trunk revision that you want to inspect',
            'Trunk',
            ['Last', 'Initial']
        ) as dlg:
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            tr = dlist.trunk
            item = dlist.SelectedItem
            if dlg.GetSelection() == 0:
                pth = item[PATH_COL] if item else tr.last_rev
                diff_after = False
            else:
                pth = tr.irev_pth(item[PATH_COL]) if item else tr.init_rev
                diff_after = True
            Async(self.__show_log, pth, diff_after, name='LogWindow')
