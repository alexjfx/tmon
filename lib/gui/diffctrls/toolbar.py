import wx

from lib.async import Async
from lib.async.lock import get_lock
from lib.async.task import ROTS
from lib.async.task import Task
from lib.async.task import interruptible
from lib.async.task import task
from lib.cmdline import cmd
from lib.gui.__frames__ import BranchDialog
from lib.gui.consts import *
from lib.gui.dialogs.commit_dialog import CommitDialog
from lib.gui.dialogs.review_dialog import ReviewDialog
from lib.gui.dialogs.trunk_edit_dialog import TrunkEditDialog
from lib.gui.diffctrls.identifiers import *
from lib.gui.utils.common import BaseToolBar
from lib.gui.utils.common import BusyInfo
from lib.gui.utils.common import ErrorDlg
from lib.gui.utils.common import InfoDlg
from lib.gui.utils.common import MessageDlg
from lib.gui.utils.common import lock_errdlg
from logging import error
from logging import info
from os.path import dirname
from os.path import exists


class DiffToolBar(BaseToolBar):

    def __init__(self, parent):
        BaseToolBar.__init__(self, parent)
        self.AddItems([
            (STARTDIFF_TOOL_ID, 'Start diff', self.__OnStartDiffToolClicked),
            (STOPDIFF_TOOL_ID, 'Stop diff', self.__OnStopDiffToolClicked),
            (BRANCH_TOOL_ID, 'Branch\tF4', self.__OnBranchToolClicked, {'enabled': True}),
            (UPDATE_TOOL_ID, 'Update\tF5', self.__OnUpdateToolClicked),
            None,
            (ACCEPT_TOOL_ID, 'Accept\tCtrl+Left', self.AcceptChanges),
            (DECLINE_TOOL_ID, 'Decline\tCtrl+Right', self.DeclineChanges),
            (RESET_TOOL_ID, 'Reset\tCtrl+R', self.ResetAcceptedChanges),
            None,
            (COMMIT_TOOL_ID, 'Commit', self.__OnCommitToolClicked),
            (REVIEW_TOOL_ID, 'Review\tF6', self.__OnReviewToolClicked),
            (REVERT_TOOL_ID, 'Revert\tF7', self.RevertChanges),
            None,
            (TERMINAL_TOOL_ID, 'Terminal\tF8', self._OnTerminalToolClicked, {'enabled': True}),
            (PROPS_TOOL_ID, 'Properties\tF11', self.__OnPropsToolClicked, {'enabled': True}),
        ])
        self.ConfigureTools()
        self.DiffTask = Task(self.__difftrunk, scheduler=self.scheduler, name='DiffTrunk')

    def ConfigureTools(self):
        vcs = self.trunk.vcs
        vcs_detected = vcs.name != 'unknown'
        rmon_auth_valid = self.rmon.auth_valid
        self.VisibleItems({
            BRANCH_TOOL_ID: vcs.implemented.get('LIST_BRANCHES', False),
            UPDATE_TOOL_ID: vcs_detected,
            COMMIT_TOOL_ID: vcs_detected,
            REVIEW_TOOL_ID: rmon_auth_valid,
            REVERT_TOOL_ID: vcs_detected,
            'Sep3': vcs_detected or rmon_auth_valid,
        })

    @get_lock()
    def __difftrunk(self):
        trunk = self.trunk
        process_path = self.dlist.ProcessPath
        rots = ROTS(self.DiffTask)
        info('Running diff of trunk "{}"...'.format(trunk.url))
        diffs = set(trunk.diff(rots))
        current_diffs = {item[PATH_COL] for _, item in self.dlist}
        if diffs != current_diffs:
            [process_path(pth) for pth in interruptible(diffs | current_diffs, rots)]
        info('Diff of trunk "{}" completed.'.format(trunk.url))

    # ================== #
    # DiffToolBar events #
    # ================== #

    def __OnStartDiffToolClicked(self, event):
        self.EnableTool(STARTDIFF_TOOL_ID, False)
        self.DiffTask.start()

    def __OnStopDiffToolClicked(self, event):
        self.EnableTool(STOPDIFF_TOOL_ID, False)
        self.DiffTask.stop()

    def __OnUpdateToolClicked(self, event):
        if not self.GetToolEnabled(UPDATE_TOOL_ID):
            return
        self.EnableTool(UPDATE_TOOL_ID, False)
        mon = self.tmon
        if not mon.started:
            return
        with BusyInfo(self.panel):
            mon.stop(True)
            mon.start()

    @task
    @get_lock(False, lock_errdlg('Cannot change the branch.'))
    def ChangeBranch(self, branch):
        self.panel.StopMonitors().wait()
        self.trunk.branch = branch
        self.upbar[BRANCH_FIELD] = branch
        self.panel.StartMonitors()

    def __OnBranchToolClicked(self, event):
        tr = self.trunk
        with BusyInfo(self.panel, 'Loading branches...'):
            branches = tr.vcs.LIST_BRANCHES()
        with BranchDialog(self.main_frame) as dlg:
            branch_combo = dlg.BranchCombo
            branch_combo.AppendItems(branches)
            curbr = tr.branch
            if curbr is None or branch_combo.FindString(curbr) == wx.NOT_FOUND:
                branch_combo.SetSelection(0)
            else:
                branch_combo.SetValue(curbr)
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            newbr = dlg.BranchCombo.GetValue()
            if curbr != newbr:
                self.ChangeBranch(newbr)

    @task
    @get_lock(False, lock_errdlg('Cannot accept changes.'))
    def AcceptChanges(self, event):
        with BusyInfo(self.panel):
            irev_pth = self.trunk.irev_pth
            process_path = self.dlist.ProcessPath
            for item in self.dlist.SelectedItems:
                pth = item[PATH_COL]
                try:
                    with open(irev_pth(pth), 'wb') as left, open(pth, 'rb') as right:
                        left.write(right.read())
                    process_path(pth)
                except (OSError, IOError) as e:
                    error(e)

    @task
    @get_lock(False, lock_errdlg('Cannot decline changes.'))
    def DeclineChanges(self, event):
        with BusyInfo(self.panel):
            irev_pth = self.trunk.irev_pth
            for item in self.dlist.SelectedItems:
                pth = item[PATH_COL]
                try:
                    with open(irev_pth(pth), 'rb') as left, open(pth, 'wb') as right:
                        right.write(left.read())
                except (OSError, IOError) as e:
                    error(e)

    @task
    @get_lock(False, lock_errdlg('Cannot reset accepted changes.'))
    def ResetAcceptedChanges(self, event):
        if MessageDlg(
            self.main_frame,
            'Are you sure that you want to reset all accepted changes?\n\n'
            'This operation is irreversible and you will need to review these changes again.',
            'Reset Accepted Changes',
            wx.YES | wx.NO | wx.ICON_EXCLAMATION
        ) == wx.ID_YES:
            with BusyInfo(self.panel):
                self.trunk.vcs.REVERT(self.trunk.init_rev, False)
                self.__OnStartDiffToolClicked(None)

    @task
    @get_lock()
    def CreateReview(self, busydlg=None, **kwargs):
        self.tmon.stop_observer()
        self.rmon.create_review(**kwargs)
        if isinstance(busydlg, BusyInfo):
            busydlg.Close()
        self.tmon.start_observer()

    def __OnReviewToolClicked(self, event):
        paths = [
            item[PATH_COL]
            for item in self.dlist.SelectedItems
            if item[STATE_COL] == MODIFIED
        ]
        if not paths:
            return

        with ReviewDialog(self.main_frame, [tr.reviewer for tr in self.trunks]) as dlg:
            dlg.ReviewerCombo.SetValue(self.trunk.reviewer)
            if dlg.ShowModal() == wx.ID_OK:
                self.CreateReview(
                    BusyInfo(self.panel),
                    reviewers=dlg.ReviewerCombo.GetParsedValue(),
                    file_list=paths,
                    task_id=dlg.TaskCombo.GetParsedValue(),
                    title=dlg.TitleCombo.GetParsedValue(),
                    params=dlg.ParamsCtrl.GetValue(),
                )

    @task
    @get_lock()
    def CommitChanges(self, paths, message, busydlg=None):
        self.tmon.stop_observer()
        commit = self.trunk.vcs.COMMIT(paths, message)
        if isinstance(busydlg, BusyInfo):
            busydlg.Close()

        result = commit.get('result')
        cid = commit.get('id')

        if not result:
            ErrorDlg(self.main_frame, 'Commit has failed.\n\nSee the log for details.')
        elif cid is not None:
            InfoDlg(
                self.main_frame, 'Commit {} is created and sent to the server.'.format(cid)
            )

        changes = commit.get('changes')
        if changes:
            process_path = self.dlist.ProcessPath
            [process_path(pth) for pth in changes]
        self.tmon.start_observer()

    def __OnCommitToolClicked(self, event):
        paths = [
            item[PATH_COL]
            for item in self.dlist.SelectedItems
            if item[STATE_COL] == MODIFIED
        ]
        if not paths:
            return

        with CommitDialog(self.main_frame) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                self.CommitChanges(
                    paths,
                    dlg.MsgCombo.GetParsedValue(),
                    BusyInfo(self.panel)
                )

    @task
    @get_lock(False, lock_errdlg('Cannot revert changes.'))
    def RevertChanges(self, event):
        with BusyInfo(self.panel):
            revert_file = self.trunk.vcs.REVERT
            for item in self.dlist.SelectedItems:
                if item[STATE_COL] == MODIFIED:
                    revert_file(item[PATH_COL])

    def _OnTerminalToolClicked(self, event, selected_item=None):
        cli = self.settings[TERM_TOOL]
        if not exists(cli):
            error('Cannot find "{}". cmd will be used instead.'.format(cli))
            cli = 'cmd'
        item = selected_item or self.dlist.SelectedItem
        pth = self.trunk.last_rev if item is None else dirname(item[PATH_COL])
        Async(cmd, cli, cwd=pth, new_console=True, hide_wnd=False, name='Terminal')

    @task
    @get_lock(False, lock_errdlg('Cannot change the local repository.'))
    def ChangeTrunkLocalRep(self, lrep):
        self.panel.StopMonitors().wait()
        self.dlist.Clear()
        self.trunk.set_local_rep(lrep)
        self.tray.DisplayBalloon('Changed local repository of trunk.', self.trunk.url)
        self.panel.StartMonitors()

    @task
    @get_lock(False, lock_errdlg('Cannot change the list of files to check/ignore dirs.'))
    def ChangeTrunkCheckParams(self, check_files, ignore_dirs):
        self.trunk.check_files = check_files
        self.trunk.ignore_dirs = ignore_dirs
        self.nbook.UpdatePageText(self.panel, len(self.dlist))
        self.upbar[WATCH_FIELD] = check_files
        self.DiffTask.start()

    def __OnPropsToolClicked(self, event):
        tr = self.trunk
        with TrunkEditDialog(self.main_frame, 'Edit Trunk') as dlg:
            dlg.UrlCombo.Enable(False)
            dlg.UrlCombo.SetValue(tr.url)
            dlg.LocalRepPicker.SetPath(tr.local_rep or '')
            dlg.CheckFilesCombo.SetValue(tr.check_files)
            dlg.IgnoreDirsCombo.SetValue(tr.ignore_dirs)
            dlg.ReviewerCombo.SetValue(tr.reviewer)
            dlg.AutoDiffCbx.SetValue(tr.auto_diff)
            dlg.VcsValue.SetLabel(tr.vcs.name)
            if dlg.ShowModal() == wx.ID_OK:
                tr.reviewer = dlg.ReviewerCombo.GetParsedValue()
                tr.auto_diff = dlg.AutoDiffCbx.GetValue()
                files = dlg.CheckFilesCombo.GetParsedValue()
                dirs = dlg.IgnoreDirsCombo.GetParsedValue()
                rep = dlg.LocalRepPicker.GetPath()
                if tr.local_rep != rep:
                    self.ChangeTrunkLocalRep(rep)
                elif tr.check_files != files or tr.ignore_dirs != dirs:
                    self.ChangeTrunkCheckParams(files, dirs)
