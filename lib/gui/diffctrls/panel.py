import wx

from lib.async.priorities import HIGH_PRIORITY
from lib.async.scheduler import TaskScheduler
from lib.async.task import TASK
from lib.async.task import task
from lib.bindings import VCS_REVIEW_BINDING
from lib.gui.consts import *
from lib.gui.diffctrls.identifiers import *
from lib.gui.diffctrls.listctrl import DiffListCtrl
from lib.gui.diffctrls.statusbar import DiffStatusBar
from lib.gui.diffctrls.toolbar import DiffToolBar
from lib.gui.utils.inheritable import InheritableAccessMixin
from lib.gui.utils.inheritable import inheritables
from lib.trunk.monitor import TrunkMonitor
from lib.vcs import init_vcs
from logging import info
from wx.lib.agw.infobar import InfoBar


@inheritables('scheduler', 'tmon', 'rmon', 'lock', 'trunk', 'tlbar', 'dlist', 'upbar', 'stbar',
              'panel', 'info',)
class DiffPanel(wx.Panel, InheritableAccessMixin):

    def __init__(self, parent, trunk):
        wx.Panel.__init__(self, parent)
        self.scheduler = TaskScheduler(name='TrunkTasks')
        self.lock = trunk.lock
        self.trunk = trunk
        self.panel = self
        self.dlist = dlist = DiffListCtrl(self)

        # create tmon and rmon
        settings = self.settings
        self.tmon = TrunkMonitor(
            trunk, process_func=dlist.ProcessPath, interval=settings[TMON],
            # callbacks={'on_done': self.__after_trunk_update},
        )
        self.__create_rmon()

        self.info = InfoBar(self)
        self.tlbar = DiffToolBar(self)

        self.upbar = upbar = DiffStatusBar(self, 210, -1)
        upbar.callback(
            WATCH_FIELD,
            lambda cfiles: 'Watching {} changes'.format('all' if cfiles else 'only local'),
            trunk.check_files
        )
        upbar.callback(BRANCH_FIELD, lambda br: 'Branch: {}'.format(br), trunk.branch)

        self.stbar = stbar = DiffStatusBar(self, (50, trunk.vcs.name), 150, 100, -1)
        stbar.callback(DIFFS_FIELD, lambda i: 'Diffs: {}'.format(i) if i else 'No diffs.', 0)
        stbar.callback(LOCK_FIELD, lambda tr: tr.lock.owner, trunk)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.info, 0, wx.EXPAND)
        sizer.Add(self.tlbar, 0, wx.EXPAND)
        sizer.Add(dlist, 1, wx.EXPAND)
        sizer.Add(upbar, 0, wx.EXPAND)
        sizer.Add(stbar, 0, wx.EXPAND)
        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)

    def __create_rmon(self):
        trunk = self.trunk
        settings = self.settings
        binding = VCS_REVIEW_BINDING[trunk.vcs]
        self.rmon = binding.cls(
            trunk, server=settings.get(binding.srv), credentials=settings[CREDS],
            process_func=self.dlist.ProcessPath, interval=settings[RMON],
            update_entire_review=binding.entire_review,
        )

    # not sure if this is required but implemented just in case vcs goes nuts
    def __after_trunk_update(self, updated_files):
        scheduled_funcs = (
            tsk.get_func()
            for tsk in self.scheduler.scheduled_tasks()
            if tsk is not None and tsk.get_name() == 'ProcessPath'
        )
        files_to_process = {f for _, (_, f), _ in scheduled_funcs} | (updated_files or set())
        outofscope_paths = {item[PATH_COL] for _, item in self.dlist} - files_to_process
        if not outofscope_paths:
            return
        info('Out-of-scope paths to be proccessed after updating trunk "{}": {}'.format(
            self.trunk.url, ', '.join(outofscope_paths)
        ))
        process_path = self.dlist.ProcessPath
        [process_path(pth) for pth in outofscope_paths]

    # =================== #
    # Monitors management #
    # =================== #

    @task
    def StartMonitors(self):
        if self.tmon.started:
            return
        self.stbar[MONITOR_FIELD] = 'Starting monitors...'
        self.rmon.start()
        self.tmon.start()
        self.stbar[MONITOR_FIELD] = 'Monitors are running.'
        if self.trunk.auto_diff:
            # wait for the completion of trunk monitor underlying task
            TASK(self.tmon).wait()
            self.tlbar.DiffTask.start()

    @task(HIGH_PRIORITY)
    def StopMonitors(self):
        self.tlbar.DiffTask.stop(True)
        if not self.tmon.started:
            return
        self.stbar[MONITOR_FIELD] = 'Stopping monitors...'
        self.tmon.stop(True)
        self.rmon.stop(True)
        self.stbar[MONITOR_FIELD] = 'Monitors are stopped.'

    @task
    def Configure(self, auth_changed=False, srv_changed=False):
        settings = self.settings
        self.tmon.interval = settings[TMON]
        self.rmon.interval = settings[RMON]
        if not auth_changed and not srv_changed:
            return

        self.StopMonitors().wait()

        trunk = self.trunk
        if auth_changed:
            self.stbar[VCS_FIELD] = init_vcs(trunk).name
            self.__create_rmon()
        elif srv_changed:
            srv = VCS_REVIEW_BINDING[trunk.vcs].srv
            self.rmon.authenticate(settings.get(srv), settings[CREDS])

        self.StartMonitors()
        self.tlbar.ConfigureTools()
