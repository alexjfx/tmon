# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Feb 14 2017)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class __TrunkEditDialog__
###########################################################################

class __TrunkEditDialog__ ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add/Edit Trunk", pos = wx.DefaultPosition, size = wx.Size( 450,270 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.UrlLbl = wx.StaticText( self, wx.ID_ANY, u"URL:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.UrlLbl.Wrap( -1 )
		PanelSizer.Add( self.UrlLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		UrlComboChoices = []
		self.UrlCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, UrlComboChoices, wx.CB_SORT|wx.TE_PROCESS_ENTER )
		PanelSizer.Add( self.UrlCombo, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.VcsLbl = wx.StaticText( self, wx.ID_ANY, u"VCS:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.VcsLbl.Wrap( -1 )
		PanelSizer.Add( self.VcsLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.VcsValue = wx.StaticText( self, wx.ID_ANY, u"unknown", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.VcsValue.Wrap( -1 )
		PanelSizer.Add( self.VcsValue, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT|wx.EXPAND, 5 )
		
		self.LocalRepLbl = wx.StaticText( self, wx.ID_ANY, u"Local repository:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.LocalRepLbl.Wrap( -1 )
		PanelSizer.Add( self.LocalRepLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_RIGHT, 5 )
		
		self.LocalRepPicker = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_SMALL )
		PanelSizer.Add( self.LocalRepPicker, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.CheckFilesLbl = wx.StaticText( self, wx.ID_ANY, u"Files to check:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.CheckFilesLbl.Wrap( -1 )
		PanelSizer.Add( self.CheckFilesLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		CheckFilesComboChoices = []
		self.CheckFilesCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, CheckFilesComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.CheckFilesCombo, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.IgnoreDirsLbl = wx.StaticText( self, wx.ID_ANY, u"Ignore dirs:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.IgnoreDirsLbl.Wrap( -1 )
		PanelSizer.Add( self.IgnoreDirsLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		IgnoreDirsComboChoices = []
		self.IgnoreDirsCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, IgnoreDirsComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.IgnoreDirsCombo, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.ReviewerLbl = wx.StaticText( self, wx.ID_ANY, u"Reviewer:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ReviewerLbl.Wrap( -1 )
		PanelSizer.Add( self.ReviewerLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		ReviewerComboChoices = []
		self.ReviewerCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, ReviewerComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.ReviewerCombo, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		
		PanelSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.AutoDiffCbx = wx.CheckBox( self, wx.ID_ANY, u"Start diff when trunk is\nloaded, created, or changed", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.AutoDiffCbx.SetValue(True) 
		PanelSizer.Add( self.AutoDiffCbx, 0, wx.ALL, 5 )
		
		
		MainSizer.Add( PanelSizer, 1, wx.EXPAND, 5 )
		
		self.StaticLine = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		MainSizer.Add( self.StaticLine, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class __SettingsDialog__
###########################################################################

class __SettingsDialog__ ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Settings", pos = wx.DefaultPosition, size = wx.Size( 450,300 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		self.NoteBook = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.GeneralPanel = wx.Panel( self.NoteBook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.TmonLbl = wx.StaticText( self.GeneralPanel, wx.ID_ANY, u"Trunk update (mins):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.TmonLbl.Wrap( -1 )
		PanelSizer.Add( self.TmonLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.TmonSpin = wx.SpinCtrl( self.GeneralPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 10000, 10 )
		PanelSizer.Add( self.TmonSpin, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.RmonLbl = wx.StaticText( self.GeneralPanel, wx.ID_ANY, u"Review check (mins):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.RmonLbl.Wrap( -1 )
		PanelSizer.Add( self.RmonLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.RmonSpin = wx.SpinCtrl( self.GeneralPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 10000, 10 )
		PanelSizer.Add( self.RmonSpin, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.DiffToolLbl = wx.StaticText( self.GeneralPanel, wx.ID_ANY, u"Diff tool:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.DiffToolLbl.Wrap( -1 )
		PanelSizer.Add( self.DiffToolLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.DiffToolPicker = wx.FilePickerCtrl( self.GeneralPanel, wx.ID_ANY, wx.EmptyString, u"Select an executable", u"*.exe", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE|wx.FLP_SMALL )
		PanelSizer.Add( self.DiffToolPicker, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.EditToolLbl = wx.StaticText( self.GeneralPanel, wx.ID_ANY, u"Text editor:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.EditToolLbl.Wrap( -1 )
		PanelSizer.Add( self.EditToolLbl, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.EditToolPicker = wx.FilePickerCtrl( self.GeneralPanel, wx.ID_ANY, wx.EmptyString, u"Select an executable", u"*.exe", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE|wx.FLP_SMALL )
		PanelSizer.Add( self.EditToolPicker, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.TermToolLbl = wx.StaticText( self.GeneralPanel, wx.ID_ANY, u"Terminal:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.TermToolLbl.Wrap( -1 )
		PanelSizer.Add( self.TermToolLbl, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.TermToolPicker = wx.FilePickerCtrl( self.GeneralPanel, wx.ID_ANY, wx.EmptyString, u"Select an executable", u"*.exe", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE|wx.FLP_SMALL )
		PanelSizer.Add( self.TermToolPicker, 0, wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		PanelSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.AutoStartCbx = wx.CheckBox( self.GeneralPanel, wx.ID_ANY, u"Run on system startup", wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer.Add( self.AutoStartCbx, 0, wx.ALL, 5 )
		
		
		PanelSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ResetLink = wx.HyperlinkCtrl( self.GeneralPanel, wx.ID_ANY, u"Reset auto-complete results", wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.HL_DEFAULT_STYLE )
		PanelSizer.Add( self.ResetLink, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.GeneralPanel.SetSizer( PanelSizer )
		self.GeneralPanel.Layout()
		PanelSizer.Fit( self.GeneralPanel )
		self.NoteBook.AddPage( self.GeneralPanel, u"General", True )
		self.ServerPanel = wx.Panel( self.NoteBook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		PanelSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer1.AddGrowableCol( 1 )
		PanelSizer1.SetFlexibleDirection( wx.BOTH )
		PanelSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.UserLbl = wx.StaticText( self.ServerPanel, wx.ID_ANY, u"User name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.UserLbl.Wrap( -1 )
		PanelSizer1.Add( self.UserLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.UserEdit = wx.TextCtrl( self.ServerPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer1.Add( self.UserEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.PassLbl = wx.StaticText( self.ServerPanel, wx.ID_ANY, u"Password:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.PassLbl.Wrap( -1 )
		PanelSizer1.Add( self.PassLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.PassEdit = wx.TextCtrl( self.ServerPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		PanelSizer1.Add( self.PassEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.stline1 = wx.StaticLine( self.ServerPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		PanelSizer1.Add( self.stline1, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.stline2 = wx.StaticLine( self.ServerPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		PanelSizer1.Add( self.stline2, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.CcollabLbl = wx.StaticText( self.ServerPanel, wx.ID_ANY, u"Collaborator:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.CcollabLbl.Wrap( -1 )
		PanelSizer1.Add( self.CcollabLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.RIGHT|wx.LEFT, 5 )
		
		self.CcollabEdit = wx.TextCtrl( self.ServerPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer1.Add( self.CcollabEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		self.BitbucketLbl = wx.StaticText( self.ServerPanel, wx.ID_ANY, u"BitBucket:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.BitbucketLbl.Wrap( -1 )
		PanelSizer1.Add( self.BitbucketLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.BitbucketEdit = wx.TextCtrl( self.ServerPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer1.Add( self.BitbucketEdit, 0, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.ServerPanel.SetSizer( PanelSizer1 )
		self.ServerPanel.Layout()
		PanelSizer1.Fit( self.ServerPanel )
		self.NoteBook.AddPage( self.ServerPanel, u"Servers", False )
		self.ToolsPanel = wx.Panel( self.NoteBook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		PanelSizer2 = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer2.AddGrowableCol( 1 )
		PanelSizer2.SetFlexibleDirection( wx.BOTH )
		PanelSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.SVNBlameLbl = wx.StaticText( self.ToolsPanel, wx.ID_ANY, u"SVN blame:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.SVNBlameLbl.Wrap( -1 )
		PanelSizer2.Add( self.SVNBlameLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.SVNBlameEdit = wx.TextCtrl( self.ToolsPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer2.Add( self.SVNBlameEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.SVNLogLbl = wx.StaticText( self.ToolsPanel, wx.ID_ANY, u"SVN log:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.SVNLogLbl.Wrap( -1 )
		PanelSizer2.Add( self.SVNLogLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.SVNLogEdit = wx.TextCtrl( self.ToolsPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer2.Add( self.SVNLogEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.stline11 = wx.StaticLine( self.ToolsPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		PanelSizer2.Add( self.stline11, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.stline21 = wx.StaticLine( self.ToolsPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		PanelSizer2.Add( self.stline21, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.GitBlameLbl = wx.StaticText( self.ToolsPanel, wx.ID_ANY, u"Git blame:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.GitBlameLbl.Wrap( -1 )
		PanelSizer2.Add( self.GitBlameLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.RIGHT|wx.LEFT, 5 )
		
		self.GitBlameEdit = wx.TextCtrl( self.ToolsPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer2.Add( self.GitBlameEdit, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		self.GitLogLbl = wx.StaticText( self.ToolsPanel, wx.ID_ANY, u"Git log:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.GitLogLbl.Wrap( -1 )
		PanelSizer2.Add( self.GitLogLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.GitLogEdit = wx.TextCtrl( self.ToolsPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer2.Add( self.GitLogEdit, 0, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.ToolsPanel.SetSizer( PanelSizer2 )
		self.ToolsPanel.Layout()
		PanelSizer2.Fit( self.ToolsPanel )
		self.NoteBook.AddPage( self.ToolsPanel, u"Tools", False )
		
		MainSizer.Add( self.NoteBook, 1, wx.EXPAND, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class __ReviewDialog__
###########################################################################

class __ReviewDialog__ ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Review", pos = wx.DefaultPosition, size = wx.Size( 320,220 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.AddGrowableRow( 3 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.TitleLbl = wx.StaticText( self, wx.ID_ANY, u"Title:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.TitleLbl.Wrap( -1 )
		PanelSizer.Add( self.TitleLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		TitleComboChoices = []
		self.TitleCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, TitleComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.TitleCombo, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.TaskLbl = wx.StaticText( self, wx.ID_ANY, u"Task ID:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.TaskLbl.Wrap( -1 )
		PanelSizer.Add( self.TaskLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		TaskComboChoices = []
		self.TaskCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, TaskComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.TaskCombo, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.ReviewerLbl = wx.StaticText( self, wx.ID_ANY, u"Reviewers:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ReviewerLbl.Wrap( -1 )
		PanelSizer.Add( self.ReviewerLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		ReviewerComboChoices = []
		self.ReviewerCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, ReviewerComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.ReviewerCombo, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.ParamsLbl = wx.StaticText( self, wx.ID_ANY, u"Parameters:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ParamsLbl.Wrap( -1 )
		PanelSizer.Add( self.ParamsLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.ParamsCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		PanelSizer.Add( self.ParamsCtrl, 0, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		
		MainSizer.Add( PanelSizer, 1, wx.EXPAND, 5 )
		
		self.StaticLine = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		MainSizer.Add( self.StaticLine, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.BOTTOM|wx.TOP|wx.EXPAND, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class __CommitDialog__
###########################################################################

class __CommitDialog__ ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Commit", pos = wx.DefaultPosition, size = wx.Size( 450,110 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.MsgLbl = wx.StaticText( self, wx.ID_ANY, u"Message:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.MsgLbl.Wrap( -1 )
		PanelSizer.Add( self.MsgLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		MsgComboChoices = []
		self.MsgCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, MsgComboChoices, wx.CB_SORT )
		PanelSizer.Add( self.MsgCombo, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		
		MainSizer.Add( PanelSizer, 1, wx.EXPAND, 5 )
		
		self.StaticLine = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		MainSizer.Add( self.StaticLine, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.BOTTOM|wx.TOP|wx.EXPAND, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class __SearchDialog__
###########################################################################

class __SearchDialog__ ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 700,500 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.DIALOG_NO_PARENT|wx.MAXIMIZE_BOX|wx.MINIMIZE_BOX|wx.RESIZE_BORDER|wx.SYSTEM_MENU )
		
		self.SetSizeHintsSz( wx.Size( 500,300 ), wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		self.MainPanel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		PanelSizer = wx.BoxSizer( wx.VERTICAL )
		
		SearchEditSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.SearchEdit = wx.SearchCtrl( self.MainPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		self.SearchEdit.ShowSearchButton( True )
		self.SearchEdit.ShowCancelButton( True )
		self.SearchEdit.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		SearchEditSizer.Add( self.SearchEdit, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		LstSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.LstLabel = wx.StaticText( self.MainPanel, wx.ID_ANY, u"Match %", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.LstLabel.Wrap( -1 )
		LstSizer.Add( self.LstLabel, 1, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 5 )
		
		self.LstPercent = wx.SpinCtrl( self.MainPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 50,-1 ), wx.SP_ARROW_KEYS, 50, 100, 100 )
		LstSizer.Add( self.LstPercent, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 5 )
		
		
		SearchEditSizer.Add( LstSizer, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		PanelSizer.Add( SearchEditSizer, 0, wx.EXPAND|wx.ALL, 5 )
		
		OptionSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.SearchPanel = wx.Panel( self.MainPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.DOUBLE_BORDER|wx.TAB_TRAVERSAL )
		SearchSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		SearchSizer.AddGrowableCol( 1 )
		SearchSizer.SetFlexibleDirection( wx.BOTH )
		SearchSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.SearchLbl = wx.StaticText( self.SearchPanel, wx.ID_ANY, u"Select where to search", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.SearchLbl.Wrap( -1 )
		SearchSizer.Add( self.SearchLbl, 0, wx.TOP|wx.LEFT, 5 )
		
		
		SearchSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.PatternsRadio = wx.RadioButton( self.SearchPanel, wx.ID_ANY, u"File regex patterns:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.PatternsRadio.SetValue( True ) 
		SearchSizer.Add( self.PatternsRadio, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.LEFT, 5 )
		
		PatternsComboChoices = []
		self.PatternsCombo = wx.ComboBox( self.SearchPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, PatternsComboChoices, wx.CB_SORT|wx.TE_PROCESS_ENTER )
		SearchSizer.Add( self.PatternsCombo, 0, wx.EXPAND|wx.TOP|wx.RIGHT, 5 )
		
		self.AllRadio = wx.RadioButton( self.SearchPanel, wx.ID_ANY, u"All non-binary files", wx.DefaultPosition, wx.DefaultSize, 0 )
		SearchSizer.Add( self.AllRadio, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.LEFT, 5 )
		
		
		SearchSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.TrunksBtn = wx.Button( self.SearchPanel, wx.ID_ANY, u"Other trunks", wx.DefaultPosition, wx.DefaultSize, 0 )
		SearchSizer.Add( self.TrunksBtn, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.ALL, 5 )
		
		self.TrunksLbl = wx.StaticText( self.SearchPanel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.TrunksLbl.Wrap( -1 )
		SearchSizer.Add( self.TrunksLbl, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )
		
		
		self.SearchPanel.SetSizer( SearchSizer )
		self.SearchPanel.Layout()
		SearchSizer.Fit( self.SearchPanel )
		OptionSizer.Add( self.SearchPanel, 1, wx.EXPAND, 5 )
		
		Sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.CsCheck = wx.CheckBox( self.MainPanel, wx.ID_ANY, u"Match case", wx.DefaultPosition, wx.DefaultSize, 0 )
		Sizer.Add( self.CsCheck, 0, wx.ALL, 5 )
		
		self.WwCheck = wx.CheckBox( self.MainPanel, wx.ID_ANY, u"Match whole word", wx.DefaultPosition, wx.DefaultSize, 0 )
		Sizer.Add( self.WwCheck, 0, wx.ALL, 5 )
		
		self.ReCheck = wx.CheckBox( self.MainPanel, wx.ID_ANY, u"Regular expression", wx.DefaultPosition, wx.DefaultSize, 0 )
		Sizer.Add( self.ReCheck, 0, wx.ALL, 5 )
		
		self.CtxCheck = wx.CheckBox( self.MainPanel, wx.ID_ANY, u"Show context", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.CtxCheck.SetValue(True) 
		Sizer.Add( self.CtxCheck, 0, wx.ALL, 5 )
		
		
		OptionSizer.Add( Sizer, 0, wx.EXPAND, 5 )
		
		
		PanelSizer.Add( OptionSizer, 0, wx.EXPAND, 5 )
		
		
		self.MainPanel.SetSizer( PanelSizer )
		self.MainPanel.Layout()
		PanelSizer.Fit( self.MainPanel )
		MainSizer.Add( self.MainPanel, 1, wx.EXPAND, 0 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class __LogDialog__
###########################################################################

class __LogDialog__ ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Log", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.FRAME_NO_TASKBAR|wx.MAXIMIZE_BOX|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 500,300 ), wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		self.LogCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_RICH|wx.TE_WORDWRAP|wx.NO_BORDER )
		self.LogCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INFOBK ) )
		
		MainSizer.Add( self.LogCtrl, 1, wx.EXPAND, 5 )
		
		FooterSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		FooterSizer.SetFlexibleDirection( wx.BOTH )
		FooterSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_NONE )
		
		self.ErrorsCbx = wx.CheckBox( self, wx.ID_ANY, u"Errors and warnings only", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ErrorsCbx.SetValue(True) 
		FooterSizer.Add( self.ErrorsCbx, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.ClearBtn = wx.Button( self, wx.ID_ANY, u"Clear log", wx.DefaultPosition, wx.DefaultSize, 0 )
		FooterSizer.Add( self.ClearBtn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		MainSizer.Add( FooterSizer, 0, wx.EXPAND, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class BranchDialog
###########################################################################

class BranchDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Select Branch", pos = wx.DefaultPosition, size = wx.Size( 450,110 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.BranchLbl = wx.StaticText( self, wx.ID_ANY, u"Branch:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.BranchLbl.Wrap( -1 )
		PanelSizer.Add( self.BranchLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		BranchComboChoices = []
		self.BranchCombo = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, BranchComboChoices, wx.CB_READONLY|wx.CB_SORT )
		PanelSizer.Add( self.BranchCombo, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		
		MainSizer.Add( PanelSizer, 1, wx.EXPAND, 5 )
		
		self.StaticLine = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		MainSizer.Add( self.StaticLine, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.BOTTOM|wx.TOP|wx.EXPAND, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

###########################################################################
## Class TrunkDeleteDialog
###########################################################################

class TrunkDeleteDialog ( wx.Dialog ):
	
	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Remove Trunk", pos = wx.DefaultPosition, size = wx.Size( 370,170 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		MainSizer = wx.BoxSizer( wx.VERTICAL )
		
		PanelSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		PanelSizer.AddGrowableCol( 1 )
		PanelSizer.AddGrowableRow( 0 )
		PanelSizer.SetFlexibleDirection( wx.BOTH )
		PanelSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.DelBmp = wx.StaticBitmap( self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_WARNING,  ), wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer.Add( self.DelBmp, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.DelLbl = wx.StaticText( self, wx.ID_ANY, u"The trunk will no longer be watched and\nwill be removed from the monitor.\nAll review data will be lost.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.DelLbl.Wrap( -1 )
		PanelSizer.Add( self.DelLbl, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		PanelSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.DelCbx = wx.CheckBox( self, wx.ID_ANY, u"Also, delete trunk files from disk.", wx.DefaultPosition, wx.DefaultSize, 0 )
		PanelSizer.Add( self.DelCbx, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		MainSizer.Add( PanelSizer, 1, wx.EXPAND, 5 )
		
		self.StaticLine = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		MainSizer.Add( self.StaticLine, 0, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		sdbSizer = wx.StdDialogButtonSizer()
		self.sdbSizerOK = wx.Button( self, wx.ID_OK )
		sdbSizer.AddButton( self.sdbSizerOK )
		self.sdbSizerCancel = wx.Button( self, wx.ID_CANCEL )
		sdbSizer.AddButton( self.sdbSizerCancel )
		sdbSizer.Realize();
		
		MainSizer.Add( sdbSizer, 0, wx.BOTTOM|wx.TOP|wx.EXPAND, 5 )
		
		
		self.SetSizer( MainSizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

