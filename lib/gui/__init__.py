from wx import Dialog

__originit = Dialog.__init__


def __init(self, *args, **kwargs):
    __originit(self, *args, **kwargs)
    # set the parent font for all dialogs
    self.SetFont(args[0].GetFont())


# substitute dialog init method
Dialog.__init__ = __init
