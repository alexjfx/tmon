from ..__frames__ import __SettingsDialog__
from ..consts import *
from ..utils.common import InfoDlg
from wx import EVT_BUTTON
from wx import EVT_HYPERLINK


class SettingsDialog(__SettingsDialog__):

    def __init__(self, parent):
        __SettingsDialog__.__init__(self, parent)

        settings = parent.settings
        self.UserEdit.Value, self.PassEdit.Value = settings[CREDS]
        self.CcollabEdit.SetValue(settings[CCOLLAB])
        self.BitbucketEdit.SetValue(settings[BITBUCKET])
        self.TmonSpin.SetValue(settings[TMON] / 60)
        self.RmonSpin.SetValue(settings[RMON] / 60)
        self.DiffToolPicker.SetPath(settings[DIFF_TOOL])
        self.EditToolPicker.SetPath(settings[EDIT_TOOL])
        self.TermToolPicker.SetPath(settings[TERM_TOOL])
        self.SVNBlameEdit.SetValue(settings[SVN_BLAME])
        self.GitBlameEdit.SetValue(settings[GIT_BLAME])
        self.SVNLogEdit.SetValue(settings[SVN_LOG])
        self.GitLogEdit.SetValue(settings[GIT_LOG])

        self.DiffToolPicker.GetTextCtrl().SetEditable(False)
        self.EditToolPicker.GetTextCtrl().SetEditable(False)
        self.TermToolPicker.GetTextCtrl().SetEditable(False)

        self.sdbSizerOK.Bind(EVT_BUTTON, self.__OnOKButtonClick)
        self.ResetLink.Bind(EVT_HYPERLINK, self.__OnResetLink)

    def __OnResetLink(self, event):
        self.Parent.settings.update({
            URLS: [],
            CHECK_FILES: [],
            IGNORE_DIRS: [],
            REVIEWERS: [],
            TASK_IDS: [],
            REVIEW_TITLES: [],
        })
        InfoDlg(self, 'Auto-complete results are reset.\n\n ')

    def __OnOKButtonClick(self, event):
        self.Parent.settings.update({
            CREDS: (self.UserEdit.GetValue().strip(), self.PassEdit.GetValue()),
            CCOLLAB: self.CcollabEdit.GetValue().strip(),
            BITBUCKET: self.BitbucketEdit.GetValue().strip(),
            TMON: self.TmonSpin.GetValue() * 60,
            RMON: self.RmonSpin.GetValue() * 60,
            DIFF_TOOL: self.DiffToolPicker.GetPath(),
            EDIT_TOOL: self.EditToolPicker.GetPath(),
            TERM_TOOL: self.TermToolPicker.GetPath(),
            SVN_BLAME: self.SVNBlameEdit.GetValue().strip(),
            GIT_BLAME: self.GitBlameEdit.GetValue().strip(),
            SVN_LOG: self.SVNLogEdit.GetValue().strip(),
            GIT_LOG: self.GitLogEdit.GetValue().strip(),
        })
        event.Skip()
