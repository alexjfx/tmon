from ..__frames__ import __CommitDialog__
from ..consts import *
from ..utils.common import SaveableCombo
from wx import EVT_BUTTON
from wx import EVT_TEXT


class CommitDialog(__CommitDialog__):

    def __init__(self, parent):
        __CommitDialog__.__init__(self, parent)
        self.MsgCombo = SaveableCombo(self.MsgCombo, parent.settings, REVIEW_TITLES)
        self.sdbSizerOK.Disable()
        self.MsgCombo.Bind(EVT_TEXT, self.__OnMsgChange)
        self.sdbSizerOK.Bind(EVT_BUTTON, self.__OnOKButtonClick)

    def __OnMsgChange(self, event):
        self.sdbSizerOK.Enable(bool(event.String.strip()))
        event.Skip()

    def __OnOKButtonClick(self, event):
        self.MsgCombo.Save()
        event.Skip()
