from ..__frames__ import __ReviewDialog__
from ..consts import *
from ..utils.common import SaveableCombo
from wx import EVT_BUTTON
from wx import EVT_TEXT


class ReviewDialog(__ReviewDialog__):

    def __init__(self, parent, reviewers=None):
        __ReviewDialog__.__init__(self, parent)
        settings = parent.settings
        self.TitleCombo = SaveableCombo(self.TitleCombo, settings, REVIEW_TITLES)
        self.TaskCombo = SaveableCombo(self.TaskCombo, settings, TASK_IDS)
        self.ReviewerCombo = SaveableCombo(
            self.ReviewerCombo, settings, REVIEWERS, reviewers or []
        )
        self.ReviewerCombo.Bind(EVT_TEXT, self.__OnReviewerChange)
        self.sdbSizerOK.Bind(EVT_BUTTON, self.__OnOKButtonClick)

    def __OnReviewerChange(self, event):
        self.sdbSizerOK.Enable(bool(event.String.strip()))
        event.Skip()

    def __OnOKButtonClick(self, event):
        self.TitleCombo.Save()
        self.TaskCombo.Save()
        self.ReviewerCombo.Save()
        event.Skip()
