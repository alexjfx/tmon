import wx

from ...vcs import detect_vcs
from ..__frames__ import __TrunkEditDialog__
from ..consts import *
from ..utils.common import ErrorDlg
from ..utils.common import MessageDlg
from ..utils.common import SaveableCombo


class TrunkEditDialog(__TrunkEditDialog__):

    def __init__(self, parent, title):
        __TrunkEditDialog__.__init__(self, parent)
        self.SetTitle(title)
        settings = parent.settings
        trunks = parent.trunks
        self.UrlCombo = SaveableCombo(
            self.UrlCombo, settings, URLS, [tr.url for tr in trunks]
        )
        self.CheckFilesCombo = SaveableCombo(
            self.CheckFilesCombo, settings, CHECK_FILES, [tr.check_files for tr in trunks]
        )
        self.IgnoreDirsCombo = SaveableCombo(
            self.IgnoreDirsCombo, settings, IGNORE_DIRS, [tr.ignore_dirs for tr in trunks]
        )
        self.ReviewerCombo = SaveableCombo(
            self.ReviewerCombo, settings, REVIEWERS, [tr.reviewer for tr in trunks]
        )
        self.LocalRepPicker.GetTextCtrl().SetEditable(False)
        urlprop_bind = self.UrlCombo.Bind
        urlprop_bind(wx.EVT_TEXT, self.__OnCtrlChange)
        urlprop_bind(wx.EVT_TEXT_ENTER, self.__OnInvokeVcsDetect)
        urlprop_bind(wx.EVT_KILL_FOCUS, self.__OnInvokeVcsDetect)
        self.LocalRepPicker.Bind(wx.EVT_DIRPICKER_CHANGED, self.__OnCtrlChange)
        self.CheckFilesCombo.Bind(wx.EVT_TEXT, self.__OnCtrlChange)
        self.sdbSizerOK.Bind(wx.EVT_BUTTON, self.__OnOKButtonClick)

    def __OnCtrlChange(self, event):
        self.sdbSizerOK.Enable(bool(
            self.UrlCombo.GetParsedValue() and self.LocalRepPicker.GetPath()
        ))
        event.Skip()

    def __OnInvokeVcsDetect(self, event):
        value = self.UrlCombo.GetValue().strip()
        if value:
            self.VcsValue.SetLabel(detect_vcs(value).name)
        event.Skip()

    def __OnOKButtonClick(self, event):
        urlprop = self.UrlCombo
        if urlprop.IsEnabled() and urlprop.GetParsedValue() in self.Parent.trunks:
            return ErrorDlg(
                self,
                'Trunk with this URL is already added.\n\nPlease type in another URL.',
            )
        if not self.CheckFilesCombo.GetParsedValue() and MessageDlg(
            self,
            'No files are specified for checking.\n\n'
            'Only local changes against VCS version will be checked.\n'
            'Continue?',
            'Warning',
            wx.YES | wx.NO | wx.ICON_WARNING
        ) == wx.ID_NO:
            return
        urlprop.Save()
        self.CheckFilesCombo.Save()
        self.IgnoreDirsCombo.Save()
        self.ReviewerCombo.Save()
        event.Skip()
