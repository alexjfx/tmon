import wx

from ...logger import register_wxconsole
from ..__frames__ import __LogDialog__
from ..utils.common import BringToFront
from ..utils.common import CloseOnEscapeMixin


class LogDialog(__LogDialog__, CloseOnEscapeMixin):

    def __init__(self, parent):
        __LogDialog__.__init__(self, parent)
        CloseOnEscapeMixin.__init__(self)
        self.SetIcon(parent.GetIcon())
        font = parent.GetFont()
        self.ClearBtn.SetFont(font)
        self.ErrorsCbx.SetFont(font)
        ctrl = self.LogCtrl
        font.SetPointSize(font.GetPointSize() - 1)
        ctrl.SetFont(font)
        register_wxconsole(self.__Write)
        ctrl.Bind(wx.EVT_TEXT, self.__OnText)
        self.ClearBtn.Bind(wx.EVT_BUTTON, self.__OnClear)
        self.Bind(wx.EVT_CLOSE, self.__OnClose)

    def __Write(self, record):
        if self.ErrorsCbx.IsChecked() and record.levelname not in {'ERROR', 'WARNING'}:
            return
        self.LogCtrl.AppendText('[%(asctime)s %(levelname)7s] %(msg)s\n' % (record.__dict__))

    def __OnText(self, event):
        try:
            if ' ERROR] ' in event.GetString().splitlines()[-1]:
                BringToFront(self)
        except IndexError:
            pass
        event.Skip()

    def __OnClear(self, event):
        self.LogCtrl.Clear()
        event.Skip()

    def __OnClose(self, event):
        self.Hide()
