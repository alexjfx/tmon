from ...consts import *
from ...diffctrls.identifiers import *
from ...diffctrls.popupmenu import DiffPopupMenu
from wx import EVT_RIGHT_DOWN


class DiffPopupMenuMixin(object):

    def __init__(self, dlist):
        self.__info = None
        self.__origd__ = self.__dlist__ = dlist
        self.menu = DiffPopupMenu(self, False)

        self.Bind(EVT_RIGHT_DOWN, self.__OnRightDown)

    def __getattr__(self, name):
        return getattr(self.__dict__['__dlist__'], name)

    def __OnRightDown(self, event):
        self.__info = info = self.CurrentLineInfo(event)
        self.__dlist__ = info['dlist'] or self.__origd__
        event.Skip()

    @property
    def SelectedItem(self):
        file_path = self.__info['file_path']
        if file_path is None:
            return
        _, item = self.__dlist__[PATH_COL, file_path]
        return self.__dlist__.CreateItemObj(file_path) if item is None else item

    @property
    def SelectedItems(self):
        item = self.SelectedItem
        return [] if item is None else [item]
