import wx

from ....async import Async
from ....cmdline import cmd
from ....search.matcher import splitter
from ...consts import *
from ...diffctrls.identifiers import *
from ...utils.thrsafe import threadsafe
from logging import error
from os.path import basename
from os.path import exists
from popup import DiffPopupMenuMixin
from wx import YieldIfNeeded
from wx.richtext import RichTextAttr
from wx.richtext import RichTextCtrl

TEXT_EDITORS = {
    'sublime_text.exe': ':',
    'notepad++.exe': ' -n',
}


class ResultList(RichTextCtrl, DiffPopupMenuMixin):

    def __init__(self, parent, dlist):
        RichTextCtrl.__init__(
            self, parent,
            style=wx.TE_READONLY | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS
        )
        self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INFOBK))
        DiffPopupMenuMixin.__init__(self, dlist)

        self.__DefStyle = RichTextAttr()

        self.__UrlStyle = st = RichTextAttr()
        st.SetTextColour(wx.BLUE)
        st.SetFontUnderlined(True)
        st.SetFontWeight(wx.FONTWEIGHT_BOLD)

        self.__MatchStyle = st = RichTextAttr()
        st.SetBackgroundColour(LIGHT_GREEN)

        self.__MpcStyle = st = RichTextAttr()
        st.SetTextColour(wx.RED)

        self.__LineNoStyle = st = RichTextAttr()
        st.SetFontWeight(wx.FONTWEIGHT_BOLD)

        self.__ContextLineStyle = st = RichTextAttr()
        st.SetTextColour((102, 102, 102))

        self.Bind(wx.EVT_TEXT_URL, self.__OnUrl)
        self.Bind(wx.EVT_LEFT_DCLICK, self.__OnDblClick)

    def __parse_fileline(self, line):
        short, url = line[:-1].rsplit(' (')
        trunk = self.trunks[url]
        return trunk.expand(short), trunk

    def __fileline(self, line_num):
        for i in range(line_num, -1, -1):
            line = self.GetLineText(i)
            if line.startswith('/'):
                return self.__parse_fileline(line)
        return None, None

    def __dlist(self, trunk):
        for panel in self.nbook.IterPages():
            if panel.trunk == trunk:
                return panel.dlist

    def CurrentLineInfo(self, event):
        position = event.GetPosition()
        _, hitpos = self.HitTestPos(position)
        _, line_num = self.PositionToXY(hitpos)
        line = self.GetLineText(line_num)
        file_path, trunk = self.__fileline(line_num)
        return {
            'line': line,
            'file_path': file_path,
            'trunk': trunk,
            'dlist': self.__dlist(trunk),
        }

    # ================ #
    # ResultList funcs #
    # ================ #

    @threadsafe
    def ClearResults(self):
        self.Clear()
        self.SetDefaultStyle(self.__DefStyle)

    @threadsafe
    def AddInfo(self, string=None):
        if string:
            self.WriteText(string)
        self.Newline()

    @threadsafe
    def AddFilePath(self, file_obj):
        path = file_obj['short']
        url = file_obj['url']
        self.BeginStyle(self.__UrlStyle)
        self.BeginURL('{} ({})'.format(path, url))
        self.WriteText(path)
        self.EndURL()
        self.EndStyle()
        self.WriteText(' ({})'.format(url))
        self.Newline()
        YieldIfNeeded()

    def __WriteFormattedLine(self, string, positions):
        WriteText = self.WriteText
        BeginStyle = self.BeginStyle
        EndStyle = self.EndStyle

        for i, dt in enumerate(splitter(string, positions)):
            if not dt:
                continue
            if i % 2 == 0:
                WriteText(dt)
                continue
            st, mpc = dt
            BeginStyle(self.__MatchStyle)
            WriteText(st)
            if mpc is not None:
                BeginStyle(self.__MpcStyle)
                WriteText(' [%d%%]' % mpc)
                EndStyle()
            EndStyle()
        self.Newline()

    @threadsafe
    def AddResult(self, data):
        WriteText = self.WriteText
        BeginStyle = self.BeginStyle
        EndStyle = self.EndStyle
        Newline = self.Newline
        WriteFormattedLine = self.__WriteFormattedLine

        self.AddFilePath(data['file_obj'])
        for line_data in data['lines']:
            YieldIfNeeded()
            __line, positions, context = line_data

            if context is None:
                # write line number
                BeginStyle(self.__LineNoStyle)
                val = '{num:>6}: ' if __line['part'] == 1 else '{num:>6} ({part}): '
                WriteText(val.format(**__line))
                EndStyle()
                WriteFormattedLine(__line['str'], positions)
                continue

            __string = __line['str']
            for line in context:
                string = line['str']

                # skip excessive context
                if string != __string and line['part'] > 1:
                    continue

                # write line number
                BeginStyle(self.__LineNoStyle)
                val = '{num:>6}: ' if line['part'] == 1 else '{num:>6} ({part}): '
                WriteText(val.format(**line))
                EndStyle()

                # write context line
                if string != __string:
                    if string != '':
                        BeginStyle(self.__ContextLineStyle)
                        WriteText(string)
                        EndStyle()
                    Newline()
                    continue
                WriteFormattedLine(string, positions)
            Newline()
        Newline()

    @threadsafe
    def FreezeCtrl(self):
        self.Freeze()

    @threadsafe
    def ThawCtrl(self):
        self.Thaw()

    def __OnUrl(self, event):
        file_path, trunk = self.__parse_fileline(event.GetString())
        self.__dlist(trunk).DiffItem(file_path)
        event.Skip()

    def __OnDblClick(self, event):
        info = self.CurrentLineInfo(event)
        if info['file_path'] is None:
            return

        num = info['line'].strip().split(':')[0].split(' (')[0]
        if not num.isdigit():
            return

        editor = self.settings[EDIT_TOOL]
        if not exists(editor):
            error('Cannot find "{}". Notepad will be used instead.'.format(editor))
            editor = 'notepad'
        line_arg = TEXT_EDITORS.get(basename(editor).lower(), '')

        Async(
            cmd, '{editor} {pth}{line_arg}{line_num}',
            editor=editor,
            pth=info['file_path'],
            line_arg=line_arg,
            line_num=num if line_arg else '',
            hide_wnd=False,
            name='Editor'
        )
