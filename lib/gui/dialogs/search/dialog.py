import wx

from ....async.scheduler import TaskScheduler
from ....async.task import TASK
from ....async.task import interruptible
from ....async.task import task
from ....common import Pluralizer
from ....search.core import search_text
from ....search.core import walk_files
from ....search.matcher import Matcher
from ...__frames__ import __SearchDialog__
from ...consts import *
from ...diffctrls.identifiers import *
from ...utils.common import BringToFront
from ...utils.common import BusyInfo
from ...utils.common import CloseOnEscapeMixin
from ...utils.common import SaveableCombo
from ...utils.inheritable import InheritableAccessMixin
from ...utils.thrsafe import threadsafe
from logging import error
from results import ResultList
from wx.lib.dialogs import MultipleChoiceDialog

LAUNCH_SEARCH_ID = wx.NewId()


def progress(iterable, pfunc, step=5):
    x = len(iterable)
    percent_map = {(x * i // 100): i for i in range(0, 101, step)}
    for pos, item in enumerate(iterable, 1):
        if pos in percent_map:
            pfunc(percent_map[pos])
        yield item


class SearchDialog(__SearchDialog__, InheritableAccessMixin, CloseOnEscapeMixin):

    def __init__(self, parent):
        __SearchDialog__.__init__(self, parent)
        CloseOnEscapeMixin.__init__(self)
        self.SetIcon(self.main_frame.GetIcon())
        self.scheduler = TaskScheduler(workers_count=2, name='SearchDlgTasks')
        self.extra_urls = ()

        trunk = parent.trunk
        self.SetTitle('{} [{}]'.format(trunk.url, trunk.md5hash))

        self.PatternsCombo = SaveableCombo(
            self.PatternsCombo, parent.settings, CHECK_FILES,
            [tr.check_files for tr in parent.trunks]
        )

        self.ResultList = ResultList(self.MainPanel, self.dlist)
        self.MainPanel.GetSizer().Add(self.ResultList, 1, wx.EXPAND, 5)
        self.CenterOnScreen()

        self.SetAcceleratorTable(wx.AcceleratorTable([
            (wx.ACCEL_CTRL, ord('F'), LAUNCH_SEARCH_ID),
        ]))

        self.Bind(wx.EVT_CLOSE, self.__OnClose)
        self.Bind(wx.EVT_MENU, self.__OnLaunchSearch, id=LAUNCH_SEARCH_ID)
        self.ReCheck.Bind(wx.EVT_CHECKBOX, self.__OnCheck)
        self.WwCheck.Bind(wx.EVT_CHECKBOX, self.__OnCheck)
        self.SearchEdit.Bind(wx.EVT_TEXT_ENTER, self.__OnSearch)
        self.SearchEdit.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.__OnSearch)
        self.PatternsCombo.Bind(wx.EVT_TEXT, self.__OnPatternsEdit)
        self.PatternsCombo.Bind(wx.EVT_TEXT_ENTER, self.__OnSearch)
        self.PatternsCombo.SetValue(trunk.check_files)
        self.TrunksBtn.Bind(wx.EVT_BUTTON, self.__OnTrunksChange)

    def __del__(self):
        self.scheduler.stop().join()

    def __OnClose(self, event):
        # clear cache results for trunk
        cache = walk_files.cache
        for key in cache.keys():
            del cache[key]

        self.Destroy()
        event.Skip()

    @threadsafe
    def CreateMatcher(self):
        string = self.SearchEdit.GetLineText(0).strip()
        if string:
            return Matcher(
                string,
                self.CsCheck.IsChecked(),
                self.WwCheck.IsChecked(),
                self.ReCheck.IsChecked(),
                self.LstPercent.GetValue(),
            )

    def YieldFiles(self, tsk=None):
        all_files = bool(threadsafe(self.AllRadio.GetValue)())
        check_files = threadsafe(self.PatternsCombo.GetValue)()

        files = {
            self.trunk: walk_files(self.trunk, all_files, check_files, tsk)
        }

        trunks = self.trunks
        for url in interruptible(self.extra_urls, tsk):
            trunk = trunks[url]
            if trunk is None:
                continue
            files[trunk] = walk_files(trunk, all_files, check_files, tsk)

        for tr, lst in files.iteritems():
            url = tr.url
            shorten = tr.shorten
            for f in lst:
                yield {
                    'file': f,
                    'url': url,
                    'short': shorten(f),
                }

    @task
    def Search(self):
        tsk = TASK(self.Search)
        context = threadsafe(self.CtxCheck.IsChecked)()
        matcher = self.CreateMatcher()
        reslist = self.ResultList
        info = reslist.AddInfo

        with BusyInfo(
            self.MainPanel,
            'Collecting files...\n[ESC to cancel]',
            (250, 70),
            tsk
        ) as wait:
            file_list = list(self.YieldFiles(tsk))
            if matcher is None:
                add = reslist.AddFilePath
                files_found = len(file_list)
                for data in interruptible(file_list, tsk):
                    add(data)
            else:
                total_matches = 0
                files_found = 0
                add = reslist.AddResult

                header = 'Searching in {:N file/s}...'.format(Pluralizer(len(file_list)))
                wait_text = header + '\n{}% done\n[ESC to cancel]'

                wait_change_text = wait.ChangeText
                pfunc = lambda p: wait_change_text(wait_text.format(p))
                info(header)
                info()

                out = search_text(progress(file_list, pfunc), matcher, context)
                for data in interruptible(out, tsk):
                    if data is None:
                        continue
                    add(data)
                    files_found += 1
                    total_matches += data['matches_found']
                    # search results threshold
                    if total_matches > 999:
                        info('Too many search results. Make search more specific.')
                        break

            if files_found == 0:
                result = 'Nothing found.'
            elif matcher is None:
                result = '{:N file/s} found.'.format(Pluralizer(files_found))
            else:
                result = '{:N match/es} across {:N file/s}.'.format(
                    Pluralizer(total_matches), Pluralizer(files_found)
                )
            info(result)
            self.tray.DisplayBalloon('Search completed.', result, lambda: BringToFront(self))

    @task
    def StartSearch(self):
        if not self.trunk.last_rev:
            error('last_rev is empty in trunk object.')
            return
        self.ResultList.FreezeCtrl()
        self.ResultList.ClearResults()
        self.Search().wait()
        self.ResultList.ThawCtrl()

    def __OnSearch(self, event):
        self.StartSearch()
        event.Skip()

    def __OnLaunchSearch(self, event):
        self.SearchEdit.SetValue(self.ResultList.GetStringSelection())
        self.StartSearch()
        event.Skip()

    def __OnPatternsEdit(self, event):
        istext = bool(event.GetString().strip())
        self.PatternsRadio.SetValue(istext)
        self.PatternsRadio.Enable(istext)
        self.AllRadio.SetValue(not istext)
        event.Skip()

    def __OnCheck(self, event):
        self.LstPercent.Enable(not self.ReCheck.IsChecked() and not self.WwCheck.IsChecked())
        event.Skip()

    def __OnTrunksChange(self, event):
        curtr = self.trunk
        trunks = [tr.url for tr in self.trunks if tr != curtr]

        with MultipleChoiceDialog(
            self, 'Select other trunks to be searched', 'Trunks', trunks, size=(500, 300),
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER,
        ) as dlg:
            lbox = dlg.lbox
            for url in self.extra_urls:
                lbox.SetSelection(lbox.FindString(url))
            if dlg.ShowModal() == wx.ID_OK:
                self.extra_urls = urls = dlg.GetValueString()
                self.TrunksLbl.SetLabel(
                    '{:N trunk/s} selected'.format(Pluralizer(len(urls))) if urls else ''
                )
        event.Skip()
