from lib.async import Async
from logging import error
from subprocess import CREATE_NEW_CONSOLE
from subprocess import PIPE
from subprocess import Popen
from subprocess import STARTF_USESHOWWINDOW
from subprocess import STARTUPINFO
from subprocess import STDOUT
from threading import Event


class __CommandLineArgs:

    def __init__(self):
        self.cmd_args = {}

    def __setitem__(self, key, value):
        data = self.cmd_args.get(key, {})
        if isinstance(value, (tuple, list)):
            data.update(value)
        else:
            data['__value__'] = value
        self.cmd_args[key] = data

    def __getargstr(self, key, cmd):
        data = self.cmd_args.get(key)
        if data is None:
            return ''
        arg = data.get(cmd)
        value = data.get('__value__')
        if arg is None or value is None:
            return ''
        return '{} "{}"'.format(arg, value)

    def __getitem__(self, key):
        if isinstance(key, tuple) and len(key) == 2:
            return self.__getargstr(*key)
        return self.cmd_args.get(key, {}).get('__value__')

    def __delitem__(self, key):
        try:
            del self.cmd_args[key]
        except KeyError:
            pass

    def __call__(self, command):
        cmd_split = command.split()
        cmd = cmd_split[0]
        getargstr = self.__getargstr
        argstr = ' '.join(getargstr(key, cmd) for key in self.cmd_args).strip()
        if argstr:
            # args are apllied right after the command itself
            return ' '.join([cmd, argstr] + cmd_split[1:])
        return command


CMD_ARGS = __CommandLineArgs()


class ExpirablePopen(Popen):

    def __init__(self, *args, **kwargs):
        self.timeout = kwargs.pop('timeout', 0)
        self.tmsg = kwargs.pop('tmsg', None) or \
            'Terminating process {pid} by timeout of {timeout} secs.'

        self.timer = None
        self.done = Event()

        Popen.__init__(self, *args, **kwargs)

    def __tkill(self):
        if not self.done.wait(self.timeout):
            error(self.tmsg.format(**self.__dict__))
            self.kill()

    def expirable(func):
        def wrapper(self, *args, **kwargs):
            # zero timeout means call of parent method
            if self.timeout == 0:
                return func(self, *args, **kwargs)

            # if timer is None, need to start it
            if self.timer is None:
                self.timer = Async(self.__tkill, daemon=True)

            result = func(self, *args, **kwargs)
            self.done.set()

            return result
        return wrapper

    wait = expirable(Popen.wait)
    communicate = expirable(Popen.communicate)


def cmd(command, **kwargs):
    kpop = kwargs.pop
    output = kpop('output', False)
    err2out = kpop('err2out', False)
    startupinfo = None
    if kpop('hide_wnd', True):
        startupinfo = STARTUPINFO()
        startupinfo.dwFlags |= STARTF_USESHOWWINDOW

    popen_kw = {
        'cwd': kpop('cwd', None),
        'timeout': kpop('timeout', 0),
        'tmsg': kpop('tmsg', None),
        'startupinfo': startupinfo,
        'creationflags': CREATE_NEW_CONSOLE if kpop('new_console', False) else 0,
    }

    cl = CMD_ARGS(command.format(**kwargs))
    try:
        if not output:
            return not bool(ExpirablePopen(cl, **popen_kw).wait())

        out, _ = ExpirablePopen(
            cl, stdout=PIPE, stderr=STDOUT if err2out else None, **popen_kw
        ).communicate()
        return out
    except OSError as e:
        error('Command failed: {}\nReason: {}'.format(cl, e))
        return '' if output else False


if __name__ == '__main__':
    check_str = 'ssh -T git@git.acronis.com'
    print repr(cmd(
        check_str, timeout=0.003,
        tmsg='Timeout expired for "{}".'.format(check_str),
    ))
