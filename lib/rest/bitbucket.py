from lib.async.timer import Timer
from lib.common import timeout
from lib.logger import debug_log
from lib.logger import log
from logging import error
from logging import info
from logging import warning
from pprint import pformat
from requests import ConnectionError
from requests import Session

REPOSITORY_SYNC_PERIOD = 600
REST = lambda typ: '{{srv}}/rest/{}/latest'.format(typ)

REST_API = REST('api')
REPOSITORY = REST_API + '/projects/{key}/repos/{slug}'
COMMITS = REPOSITORY + '/commits'
PULL_REQUESTS = REST_API + '/projects/{origin_key}/repos/{origin_slug}/pull-requests'
PULL_REQUEST = PULL_REQUESTS + '/{id}'
MERGE_PULL_REQUEST = PULL_REQUEST + '/merge'
PR_TEMPLATE = {
    "title": None,
    "state": "OPEN",
    "open": True,
    "closed": False,
    "fromRef": {
        "id": None,
        "repository": {
            "slug": None,
            "project": {
                "key": None,
            },
        },
    },
    "toRef": {
        "id": None,
        "repository": {
            "slug": None,
            "project": {
                "key": None,
            },
        },
    },
    "locked": False,
}

SYNC_API = REST('sync')
REPOSITORY_TO_SYNC = SYNC_API + '/projects/{key}/repos/{slug}'
SYNC_REPOSITORY = REPOSITORY_TO_SYNC + '/synchronize'

SSH_KEYS = REST('ssh') + '/keys'


def bitbucket_json(func):
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        try:
            json_obj = response.json()
        except ValueError:
            if response.text:
                error('{} [{} {}] failed.\n{}'.format(func, args, kwargs, pformat(response.text)))
            return {}

        error_messages = json_obj.get('errors')
        if error_messages is not None:
            error('{} [{} {}] failed.\n{}'.format(func, args, kwargs, pformat(error_messages)))

        return json_obj
    return wrapper


class BitbucketError(Exception):
    pass


# TODO: move REST API commands inside BitbucketSession
# parse server from url
class BitbucketSession(Session):

    def __init__(self, server=None, auth=None, url=None):
        Session.__init__(self)
        self.headers['X-Atlassian-Token'] = 'nocheck'
        self.server = server
        self.auth = auth
        self.url = url

    def __getinfo(self, url):
        if url is None:
            return
        parsed = url.split('/')
        if len(parsed) < 3:
            error('"{}" is not a valid URL.'.format(url))
            return
        rep = parsed[-1]
        try:
            rest_url = REPOSITORY.format(
                srv=self.server,
                key=parsed[-2],
                slug=rep[:-4] if rep.endswith('.git') else rep
            )
            info = self.get(rest_url).json()
        except Exception as e:
            error('Failed to get data from {} ({}): {}'.format(rest_url, url, e))
            return None
        return info if info.get('errors') is None else None

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, value):
        self.__url = value
        self.__info = self.__getinfo(value)

    @property
    def repository(self):
        info = self.__info
        if info is None:
            return {
                'srv': self.server,
                'valid': False,
                'key': '',
                'slug': '',
                'origin_key': '',
                'origin_slug': '',
            }
        return {
            'srv': self.server,
            'valid': True,
            'key': info['project']['key'],
            'slug': info['slug'],
            'origin_key': info.get('origin', info)['project']['key'],
            'origin_slug': info.get('origin', info)['slug'],
        }

    @bitbucket_json
    def rest(self, method, endpoint, spec=None, **kwargs):
        spec = spec or {}
        for k, v in self.repository.iteritems():
            if k not in spec:
                spec[k] = v
        return self.request(method, endpoint.format(**spec), **kwargs)

    def __getitem__(self, id):
        return self.rest('GET', PULL_REQUEST, {'id': id})

    def __call__(self, endpoint, spec=None, **kwargs):
        for value in self.rest('GET', endpoint, spec, **kwargs).get('values', []):
            yield value

    @log(
        'Registering SSH key from "{key_file}" at {self.server}',
        based_on_return=True,
        true='Registered SSH key from "{key_file}" at {self.server}',
        false='Cannot register SSH key from "{key_file}" at {self.server}',
    )
    def register_ssh_key(self, key_file):
        try:
            keys = {key.get('text') for key in self(SSH_KEYS)}
        except ConnectionError as e:
            error('Cannot get SSH keys from {}: {}'.format(self.server, e))
            return False

        try:
            with open(key_file, 'rb') as fd:
                key = fd.read().strip()
        except (IOError, OSError) as e:
            error('Cannot read "{}": {}'.format(key_file, e))
            return False

        if key in keys:
            return True

        try:
            return bool(self.rest('POST', SSH_KEYS, json={'text': key}).get('text'))
        except ConnectionError as e:
            error('Cannot send SSH key to {}: {}'.format(self.server, e))
            return False

    def create_pull_request(self, title, reviewers=None, source='text_review', target='master'):
        repository = self.repository

        PR_TEMPLATE['title'] = title
        PR_TEMPLATE['reviewers'] = [{'user': {'name': rev}} for rev in (reviewers or [])]

        PR_TEMPLATE['fromRef']['id'] = source
        PR_TEMPLATE['fromRef']['repository']['slug'] = repository['slug']
        PR_TEMPLATE['fromRef']['repository']['project']['key'] = repository['key']

        PR_TEMPLATE['toRef']['id'] = target
        PR_TEMPLATE['toRef']['repository']['slug'] = repository['origin_slug']
        PR_TEMPLATE['toRef']['repository']['project']['key'] = repository['origin_key']

        return self.rest('POST', PULL_REQUESTS, json=PR_TEMPLATE)

    def merge_pull_request(self, id, check_only=True):
        if check_only:
            return self.rest('GET', MERGE_PULL_REQUEST, {'id': id})

        params = {}
        for _ in timeout(1, 10, 'Merge timeout expired for pull request #{}.'.format(id)):
            params['version'] = self[id].get('version', -1)
            res = self.rest('POST', MERGE_PULL_REQUEST, {'id': id}, params=params)
            errors = res.get('errors')
            if errors is None:
                return res
            exc = str(errors[0].get('exceptionName'))
            if not exc.endswith('PullRequestOutOfDateException'):
                return res
            info('Retrying merge of pull request #{}...'.format(id))

        return res

    def fork_repository(self):
        repository = self.repository
        if not repository['key']:
            raise BitbucketError('Cannot fork "{}".'.format(self.url))
        if repository['key'] != repository['origin_key']:
            raise BitbucketError('"{}" is already forked.'.format(self.url))
        res = self.rest('POST', REPOSITORY, json={'slug': repository['slug']})
        errors = res.get('errors')
        if errors is not None:
            raise BitbucketError(errors[0]['message'])

        forked_spec = {
            'key': res['project']['key'],
            'slug': res['slug'],
        }
        self.rest('PUT', REPOSITORY, forked_spec, json={'forkable': False, 'public': False})
        self.rest('POST', REPOSITORY_TO_SYNC, forked_spec, json={'enabled': True})

        prefix = self.url.split('://', 1)[0]
        for data in res['links']['clone']:
            if data['name'] == prefix:
                return data['href']

        raise BitbucketError('Cannot get the forked repository URL.')


class SyncedBitbucketSession(BitbucketSession, Timer):

    def __init__(self, interval=REPOSITORY_SYNC_PERIOD, auto_start=False, **kwargs):
        BitbucketSession.__init__(self, **kwargs)
        Timer.__init__(self, interval, auto_start)

    @debug_log(
        'Stopping timer of {self} during session close...',
        'Stopped timer of {self} during session close.',
    )
    def close(self):
        self.stop(True)
        BitbucketSession.close(self)

    def handler(self):
        if not self.repository['valid']:
            return
        url = self.url
        for ref in self.rest('GET', REPOSITORY_TO_SYNC).get('divergedRefs', []):
            warning('Ref "{}" in {} diverged. Will be synced.'.format(ref['id'], url))
            self.rest('POST', SYNC_REPOSITORY, json={
                'refId': ref['id'],
                'action': 'DISCARD',
            })
