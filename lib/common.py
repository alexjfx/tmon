import sys

from cPickle import dump
from cPickle import load
from functools import wraps
from hashlib import md5
from logging import error
from logging import warning
from os import chmod
from os import environ
from os import makedirs
from os import remove
from os import utime
from os.path import basename
from os.path import dirname
from os.path import exists
from os.path import normpath
from os.path import sep
from os.path import splitext
from pprint import pformat
from re import compile
from shutil import rmtree
from stat import S_IWRITE
from time import sleep

__regex_match_funcs = {}
APP_DIR = dirname(sys.argv[0])


class Representable(object):

    def __repr__(self):
        return '%s (%s)' % (self.__class__.__name__, pformat(self.__dict__))


class SaveableDict(dict):

    def __init__(self, file_path, elements=None):
        dict.__init__(self, elements or {})
        path, ext = splitext(file_path)
        sign = '{USERDOMAIN}+{USERNAME}@{COMPUTERNAME}'.format(**environ)
        self.__file = '{path}[{sign}]{ext}'.format(**locals())
        try:
            with open(touch(self.__file), 'rb') as fd:
                dict.update(self, load(fd))
        except EOFError:
            warning('File "{}" is empty.'.format(file_path))
        except (OSError, IOError) as e:
            error('Cannot load data from "{}": {}'.format(file_path, e))

    def save(self):
        try:
            with open(self.__file, 'wb') as fd:
                dump(self, fd)
        except (OSError, IOError) as e:
            error('Cannot save data to "{}": {}'.format(self.__file, e))

    def update(self, elements):
        dict.update(self, elements)
        self.save()

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        self.save()

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        self.save()


class Pluralizer:

    def __init__(self, value):
        self.__value = value

    def __format__(self, formatter):
        value = self.__value
        formatter = formatter.replace('N', str(value))
        start, _, suffixes = formatter.partition('/')
        singular, _, plural = suffixes.rpartition('/')

        return '{}{}'.format(start, singular if value == 1 else plural)


def hexhash(obj, isfile=False):
    if not isfile:
        return md5(obj).hexdigest()
    try:
        with open(obj, 'rb') as fd:
            return md5(fd.read()).hexdigest()
    except (OSError, IOError) as e:
        error('Cannot get hash of "{}": {}'.format(obj, e))


def make_dirs(pth):
    if not exists(pth):
        makedirs(pth)
    return pth


def touch(file_path):
    try:
        # recreate dir structure
        pth = dirname(file_path)
        if pth and not exists(pth):
            makedirs(pth)
        # create file if not exists and touch it
        with open(file_path, 'a'):
            utime(file_path, None)
    except (OSError, IOError) as e:
        error('Cannot touch "{}": {}'.format(file_path, e))
    return file_path


def delete_file(path):
    try:
        remove(path)
    except (OSError, IOError) as e:
        error('Cannot delete "{}": {}'.format(path, e))


def __delrw__(action, name, exc):
    try:
        chmod(name, S_IWRITE)
        remove(name)
    except (OSError, IOError) as e:
        error('Cannot delete "{}": {}'.format(name, e))


def delete_dir(path):
    rmtree(path, onerror=__delrw__)


def join_paths(*paths):
    return normpath(sep.join(paths))


# file_patterns must be str of format 'ptn1, ptn2, ptn3'
# TODO: regexp pattern matching by contents
def match_file_pattern(path, file_patterns, isfile=False):
    try:
        match_funcs = __regex_match_funcs[file_patterns]
    except KeyError:
        match_funcs = {compile(f + '$').match for f in file_patterns.split(', ')}
        __regex_match_funcs[file_patterns] = match_funcs

    f = path if isfile else basename(path)
    return any(match(f) for match in match_funcs)


def cached(key_getter=lambda *args, **kwargs: None):

    __CACHE__ = {}

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            key = key_getter(*args, **kwargs)
            if key in __CACHE__:
                return __CACHE__[key]
            __CACHE__[key] = res = func(*args, **kwargs)
            return res
        setattr(wrapper, 'cache', __CACHE__)
        return wrapper
    return decorator


def timeout(interval, count, expired_msg):
    attempt = 0
    while attempt < count:
        sleep(interval)
        yield attempt
        attempt += 1
    error(expired_msg)
