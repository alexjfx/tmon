from lib.async.lock import ReentrantLock
from lib.cmdline import cmd
from lib.common import APP_DIR
from lib.common import delete_file
from lib.review.generic import *
from logging import error

REVIEW_XML = APP_DIR + '/config/review-{}.xml'
TEMPLATE_XML = APP_DIR + '/config/template.xml'


class CcollabMonitor(ReviewMonitor):

    __auth__ = {
        'creds': None,
        'valid': False,
    }

    def __init__(self, trunk, **kwargs):
        self.__auth_lock = ReentrantLock()
        ReviewMonitor.__init__(self, trunk, **kwargs)
        self.__TXMLFILE = REVIEW_XML.format(trunk.md5hash)

    def __authenticate__(self, server, (user, password)):
        with self.__auth_lock:
            auth = CcollabMonitor.__auth__
            if auth['creds'] == (server, user, password):
                return auth['valid']
            auth['creds'] = server, user, password
            auth['valid'] = valid = cmd(
                'ccollab --non-interactive login {server} {user} {password}', **locals()
            )
            return valid

    def __create__(self, reviewers, files_to_review, task_id, title, final='', svnpaths='', **_):
        if reviewers is None:
            error('Cannot create review without reviewers.')
            return
        author = CcollabMonitor.__auth__['creds'][1]
        if author is None:
            error('Cannot create review without author.')
            return

        try:
            with open(TEMPLATE_XML, 'rb') as src, open(self.__TXMLFILE, 'wb') as dst:
                dst.write(src.read().format(
                    author=author,
                    reviewers='\n'.join([
                        '<participant>reviewer=%s</participant>' % r for r in reviewers
                    ]),
                    file_list='\n'.join([
                        '<file-spec>%s</file-spec>' % f for f in files_to_review
                    ]),
                    jira_issue=task_id or 'A-1',
                    final_review=final.lower() if final else 'no',
                    svnpaths=svnpaths.replace('/,', '\n').replace(',', '\n'),
                    review_title=title or 'Texts reviewed',
                ))
        except (OSError, IOError) as e:
            error('Cannot create review template: {}'.format(e))
            return

        result = cmd('ccollab admin batch {file}', file=self.__TXMLFILE, output=True)
        delete_file(self.__TXMLFILE)
        for line in result.splitlines():
            if line.lower().startswith('review #'):
                # Review #281272 (due in 1 day) is now in the \'Inspection\' Phase
                return int(line.split()[1][1:])

    def __revstat__(self, rev_id):
        phase = cmd(
            'ccollab admin review-xml {id} --xpath /reviews/review/general/phase/@phaseId',
            id=rev_id, output=True
        )
        if phase in '23':  # inspection and fix defects
            return OPEN_REVIEW
        if phase in '67':  # cancelled and rejected
            return DENIED_REVIEW
        if phase == '5':  # completed
            pth = self.reviews[rev_id].keys()[0]
            if self.trunk.vcs.COMMITTED(pth, rev_id):
                return COMMITTED_REVIEW
            else:
                return REVIEW_TO_COMMIT

    def __update__(self, files, rev_id):
        if len(files) > 1:
            error('Cannot update multiple files in review #{}: {}'.format(
                rev_id, ', '.join(files))
            )
            return False
        st = self.__revstat__(rev_id)
        if st not in {OPEN_REVIEW, REVIEW_TO_COMMIT}:
            error('Cannot update review #{}. Only open or completed but not committed reviews '
                  'can be updated.'.format(rev_id))
            return False
        if st == REVIEW_TO_COMMIT and not cmd('ccollab admin review reopen {id}', id=rev_id):
            error('ccollab failed to reopen review #{}. No update will be made.'.format(rev_id))
            return False
        return cmd('ccollab --non-interactive addchanges {id} {pth}', id=rev_id, pth=files[0])

    def __commit__(self, rev_id):
        return cmd('ccollab commit --comment fixed:r{id} {id}', id=rev_id)

    def __revurl__(self, rev_id):
        server_str = cmd('ccollab info', output=True).splitlines()[0]
        return '{}/ui#review:id={}'.format(server_str.split()[-1], rev_id)
