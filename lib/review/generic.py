from lib.async.lock import get_lock
from lib.async.task import ROTS
from lib.async.task import interruptible
from lib.async.timer import Monitor
from lib.common import hexhash
from lib.logger import log
from logging import error
from logging import info
from re import sub

# review statuses
OPEN_REVIEW = 'open'
DENIED_REVIEW = 'denied'
REVIEW_TO_COMMIT = 'to commit'
COMMITTED_REVIEW = 'committed'


def auth_required(func):
    def wrapper(self, *args, **kwargs):
        if not self.auth_valid:
            return
        return func(self, *args, **kwargs)
    return wrapper


class ReviewMonitor(Monitor):

    def __init__(self, trunk, server=None, credentials=None, update_entire_review=False,
                 **kwargs):
        Monitor.__init__(self, trunk, **kwargs)
        self.__auth_valid = False
        self.reviews = trunk.__reviews__
        self.authenticate(server, credentials)
        self.update_entire_review = update_entire_review

    def __getattr__(self, name):
        error('Method "{}" is not implemented in {}.'.format(name, self))
        return lambda *args, **kwargs: None

    # returns True or False
    def __authenticate__(self, server, credentials):
        return

    # returns None or review_id of type int
    def __create__(self, reviewers, files_to_review, task_id, title, **_):
        return

    # returns None or review status
    def __revstat__(self, rev_id):
        return

    # returns True or False
    def __update__(self, files, rev_id):
        return

    # returns True or False
    def __commit__(self, rev_id):
        return

    # returns None or string
    def __revurl__(self, rev_id):
        return

    @log(
        'Authenticating to {self.trunk.url} via {server} as "{creds[0]}"...',
        based_on_return=True,
        true='Successfully authenticated to {self.trunk.url} via {server} as "{creds[0]}".',
        false=(
            'Cannot authenticate to {self.trunk.url} via {server} as "{creds[0]}". '
            'Review functionality will be unavailable.'
        ),
    )
    def authenticate(self, server, creds):
        self.__auth_valid = res = self.__authenticate__(server, creds)
        return res

    @property
    def auth_valid(self):
        return self.__auth_valid

    def review_id(self, file_path):
        for rev_id, files in self.reviews.items():
            if file_path in files:
                return rev_id

    def review_url(self, rev_id):
        return self.__revurl__(rev_id) or ''

    def start(self):
        process_func = self.process_func
        for _, files in self.reviews.iteritems():
            for file_path in files:
                process_func(file_path)
        Monitor.start(self)

    @auth_required
    @get_lock()
    def create_review(self, reviewers='', file_list=None, task_id='', title='', params=''):
        files_to_review = {}
        for f in (file_list or []):
            rev_id = self.review_id(f)
            if rev_id is None:
                files_to_review[f] = hexhash(f, True)
            else:
                error('File "{}" skipped because found in open review #{}.'.format(f, rev_id))
        if not files_to_review:
            error('Cannot create review because file list is empty.')
            return

        info('Creating review...')
        pdict = {
            x[0].lower(): x[1]
            for x in [p.split('=') for p in sub(r'\s', '', params).split(';')]
            if len(x) == 2
        }
        rlist = reviewers.split(', ') if reviewers else None
        rev_id = self.__create__(rlist, files_to_review, task_id, title, **pdict)
        if rev_id is None:
            error('Failed to properly create review.')
            return

        info('Created review #{} is being monitored.'.format(rev_id))
        existing_review_files = self.reviews.get(rev_id)
        self.reviews[rev_id] = files_to_review
        process_func = self.process_func
        [process_func(f) for f in files_to_review]
        if existing_review_files is not None:
            [process_func(f) for f in existing_review_files]

        return rev_id

    @auth_required
    @get_lock()
    def update_review(self, file_path, rev_id=None):
        rev_id = rev_id or self.review_id(file_path)
        if rev_id is None:
            return

        try:
            review_files = self.reviews[rev_id]
        except KeyError:
            # this check is required if passed rev_id is not None
            return

        update_entire_review = self.update_entire_review
        file_list = review_files.keys() if update_entire_review else [file_path]

        new_hashes = []
        for f in file_list:
            h = hexhash(f, True)
            if h is None:
                error(
                    'Failed to get hash of file "{}" which is on review #{}. '
                    'The review will be updated after access to '
                    'the file is restored.'.format(f, rev_id)
                )
                return
            new_hashes.append((f, h))

        to_update = {f: h for f, h in new_hashes if h != review_files[f]}
        if not to_update:
            return
        if update_entire_review:
            to_update.update(new_hashes)

        files = to_update.keys()
        if self.__update__(files, rev_id):
            for file_path, new_hash in to_update.iteritems():
                review_files[file_path] = new_hash
            info('Files updated in review #{}: {}'.format(rev_id, ', '.join(files)))
        else:
            error('Failed to update files in review #{}: {}'.format(rev_id, ', '.join(files)))

    @auth_required
    @get_lock()
    def unwatch_review(self, rev_id):
        try:
            files = self.reviews.pop(rev_id)
            info('Review #{} is no more monitored.'.format(rev_id))
        except KeyError:
            return
        process_func = self.process_func
        [process_func(pth) for pth in interruptible(files, ROTS(self))]

    def __update_and_sync(self):
        trunk = self.trunk
        info('Updating trunk "{}"...'.format(trunk.url))
        # unresolved issue: file on review been deleted by another review and
        # then deleted from local rep after update
        updated = {f for st, f in trunk.vcs.UPDATE() if st != 'D'}
        info('Uploading updated files to reviews...')
        update_review = self.update_review
        update_entire_review = self.update_entire_review
        for rev_id, files in self.reviews.iteritems():
            for file_path in files:
                if file_path not in updated:
                    continue
                update_review(file_path, rev_id)
                if update_entire_review:
                    break

    @auth_required
    @get_lock()
    def handler(self):
        reviews = self.reviews
        if not reviews:
            return

        return_on_monitor_stop = ROTS(self)
        process_func = self.process_func
        revstat = self.__revstat__
        commit = self.__commit__
        upsync = self.__update_and_sync

        for rev_id, files in interruptible(reviews.items(), return_on_monitor_stop):
            rev_status = revstat(rev_id)
            if rev_status == OPEN_REVIEW:
                continue
            elif rev_status in {DENIED_REVIEW, COMMITTED_REVIEW}:
                info('Review #{} is {} and will not be monitored.'.format(rev_id, rev_status))
            elif rev_status == REVIEW_TO_COMMIT:
                info('Committing review #{}...'.format(rev_id))
                if not commit(rev_id):
                    error('Failed to commit review #{}.'.format(rev_id))
                    return_on_monitor_stop()
                    upsync()
                    return_on_monitor_stop()
                    if revstat(rev_id) == OPEN_REVIEW:
                        info('Review #{} is open again after update.'.format(rev_id))
                        continue

            del reviews[rev_id]
            info('Review #{} is no more monitored.'.format(rev_id))
            [process_func(pth) for pth in interruptible(files, return_on_monitor_stop)]
