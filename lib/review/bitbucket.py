from lib.common import timeout
from lib.rest.bitbucket import COMMITS
from lib.rest.bitbucket import SyncedBitbucketSession
from lib.review.generic import *
from logging import error


class BitbucketMonitor(ReviewMonitor):

    def __init__(self, trunk, **kwargs):
        self.bitbucket = None
        ReviewMonitor.__init__(self, trunk, **kwargs)
        self.rev_branches = lambda: trunk.vcs.REVIEW_BRANCHES()

    def stop(self, wait=False):
        if self.bitbucket is not None:
            self.bitbucket.stop(wait)
        ReviewMonitor.stop(self, wait)

    def __commit_and_process(self, *args, **kwargs):
        commit = self.trunk.vcs.COMMIT(*args, **kwargs)
        process_func = self.process_func
        [process_func(pth) for pth in commit.get('changes', [])]
        return commit

    # TODO: check what happens when __update_after_prsync is in progress and
    # user modifies file on review - actually, nothing should happen, update_review will be
    # just dropped
    # also, check limit on COMMITS
    def __update_after_prsync(self, rev_id):
        MERGE_MESSAGE = 'merge pull request #{}'.format(rev_id)

        info('Waiting for review #{} sync with remote user trunk...'.format(rev_id))
        for _ in timeout(10, 10, 'Sync timeout expired for review #{}.'.format(rev_id)):
            for commit in self.bitbucket(COMMITS):
                if not commit['message'].lower().startswith(MERGE_MESSAGE):
                    continue
                info('Review #{} synced.'.format(rev_id))
                # update here is required to bring local trunk state with remote trunk state
                return list(self.trunk.vcs.UPDATE())

    def __authenticate__(self, server, credentials):
        url = self.trunk.url
        bb = SyncedBitbucketSession(server=server, auth=credentials, url=url)
        valid = bb.repository['valid']
        if self.bitbucket is not None:
            self.bitbucket.stop(True)
        if valid:
            self.bitbucket = bb
            bb.start()
        else:
            self.bitbucket = None
            bb.close()
        return valid

    def __create__(self, reviewers, files_to_review, task_id, title, **_):
        title = (title or 'Texts reviewed') + (' #' + task_id if task_id else '')
        commit = self.__commit_and_process(files_to_review.keys(), title)
        if commit.get('id') is None:
            return

        pr = self.bitbucket.create_pull_request(title, reviewers, **self.rev_branches())
        errors = pr.get('errors')
        if errors is None:
            return pr.get('id')

        for obj in errors:
            try:
                rev_id = obj['existingPullRequest']['id']
                info('Files will be added to the existing review #{}.'.format(rev_id))
                return rev_id
            except KeyError:
                pass

    def __revstat__(self, rev_id):
        pr = self.bitbucket[rev_id]
        state = pr.get('state')
        if state == 'DECLINED':
            return DENIED_REVIEW
        elif state == 'MERGED':
            self.__update_after_prsync(rev_id)
            return COMMITTED_REVIEW
        elif state == 'OPEN':
            for reviewer in pr['reviewers']:
                if reviewer['approved']:
                    break
            else:
                return OPEN_REVIEW

            merge_test = self.bitbucket.merge_pull_request(rev_id)
            if merge_test.get('canMerge'):
                return REVIEW_TO_COMMIT

            if merge_test.get('vetoes') is not None:
                return OPEN_REVIEW

    def __update__(self, files, rev_id):
        pr = self.bitbucket[rev_id]
        state = pr.get('state')
        if state is None:
            error('Failed to get the state of review #{}.'.format(rev_id))
            return False
        if state in {'DECLINED', 'MERGED'}:
            error('Cannot update review #{} because it is not open.'.format(rev_id))
            return False
        return bool(self.__commit_and_process(files, pr['title']).get('id'))

    def __commit__(self, rev_id):
        title = self.bitbucket[rev_id].get('title', 'Texts reviewed')
        self.__commit_and_process(self.reviews[rev_id].keys(), title)
        result = self.bitbucket.merge_pull_request(rev_id, False).get('state') == 'MERGED'
        if result:
            self.__update_after_prsync(rev_id)
        return result

    def __revurl__(self, rev_id):
        links = self.bitbucket[rev_id].get('links')
        if links is not None:
            return links['self'][0]['href']
