from lib.async.lock import get_lock
from lib.vcs.git import GitCommands
from lib.vcs.svn import SvnCommands
from lib.vcs.unknown import UnknownCommands
from logging import info


def detect_vcs(url):
    if SvnCommands.IS_VALID(url):
        return SvnCommands
    elif GitCommands.IS_VALID(url):
        return GitCommands
    return UnknownCommands


@get_lock()
def init_vcs(trunk):
    url = trunk.url
    trunk.vcs = vcs = detect_vcs(url)(trunk)
    info('VCS of trunk "%s" is %s.' % (url, vcs.name))
    return vcs
