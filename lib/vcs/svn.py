from lib.cmdline import cmd
from lib.common import delete_dir
from lib.common import join_paths
from lib.vcs.generic import Commands
from logging import error
from logging import info
from os.path import isdir
from os.path import isfile


class SvnCommands(Commands):

    name = 'SVN'

    @classmethod
    def cleanup(cls, pth):
        info('Cleanup: {}'.format(pth))
        # collect externals
        res = cmd('svn propget svn:externals -R {pth}', pth=pth, output=True)
        walker = (
            join_paths(pth, x[-1])
            for x in (line.split() for line in res.splitlines()) if x
        )
        # run cleanup for all paths
        cmd('svn cleanup {pth}', pth=pth)
        for p in walker:
            if not isdir(p):
                continue
            info('Cleanup: {}'.format(p))
            if not cmd('svn cleanup {pth}', pth=p):
                delete_dir(p)

    @classmethod
    def update(cls, pth):
        command = 'svn update {pth} --accept tf'
        out = [
            line.split()
            for line in cmd(command, pth=pth, output=True, err2out=True).splitlines()
        ]
        for dt in out:
            if not dt:
                continue
            st = ' '.join(dt)
            if st.startswith('svn: E'):
                error('Failed to update "{}".\n{}'.format(pth, st))
                return []
        return out

    @classmethod
    def IS_VALID(cls, pth):
        info = cmd('svn info --xml {pth}', pth=pth, output=True)
        return '<url>' in info and '</url>' in info

    def DOWNLOAD(self, pth):
        self.cleanup(pth)
        cmd('svn checkout {url} {pth}', url=self.trunk.url, pth=pth)

    def UPDATE(self):
        pth = self.trunk.last_rev
        out = self.update(pth)
        if not out:
            self.cleanup(pth)
            info('Update retry: {}'.format(pth))
            out = self.update(pth)
        for x in out:
            if len(x) == 2 and len(x[0]) == 1 and isfile(x[-1]):
                yield x[0], x[-1]

    def REVERT(self, pth, isfile=True):
        cmd('svn revert {rec} {pth}', rec='' if isfile else '-R', pth=pth)

    def GET_FILE_LASTREV(self, pth, dest):
        url = self.URL(pth)
        if url is not None:
            return cmd('svn export {url} {dest} --force', url=url, dest=dest)

    def MODIFIED(self, pth):
        return {
            line.split()[-1]
            for line in cmd('svn status {pth}', pth=pth, output=True).splitlines()
            if line and line[0] == 'M'
        }

    def COMMITTED(self, pth, rev_id):
        for line in cmd('svn log {pth} --xml -l 10', pth=pth, output=True).splitlines():
            if 'fixed: r{}'.format(rev_id) in line:
                return True

    def URL(self, pth):
        info = cmd('svn info --xml {pth}', pth=pth, output=True)
        if '<url>' in info and '</url>' in info:
            return info[info.find('<url>') + 5:info.find('</url>')]

    def COMMAND(self, cmd_str, pth):
        cmd(cmd_str.format(pth), hide_wnd=False)
