from lib.vcs.generic import Commands


class UnknownCommands(Commands):

    name = 'unknown'

    @classmethod
    def IS_VALID(cls, pth):
        return True
