from lib.cmdline import cmd
from lib.common import join_paths
from lib.logger import AS_INFO
from lib.logger import log
from lib.vcs.generic import Commands
from logging import error
from logging import info
from os.path import isdir
from os.path import isfile
from os.path import split
from re import MULTILINE
from re import compile

DEF = 'origin/master'
FIX = 'text_review'

mc_pattern_repl1 = compile(r'^=======[\s\S]+?>>>>>>>.+\n', MULTILINE).subn
mc_pattern_repl2 = compile(r'^<<<<<<<.+\n', MULTILINE).subn


class GitCommands(Commands):

    name = 'Git'
    implemented = {
        'LIST_BRANCHES': True,
        'COMMIT': True,
    }

    def __init__(self, *args, **kwargs):
        Commands.__init__(self, *args, **kwargs)
        if self.trunk.branch is None:
            self.trunk.branch = DEF

    @classmethod
    @log('Getting current branch in "{pth}"...', 'Current branch in "{pth}" is {__result__}.')
    def current_branch(cls, pth):
        branches = cmd('git branch -vv', cwd=pth, output=True).splitlines()
        for line in branches:
            if not line.startswith('*'):
                continue
            spl = line.split()
            remote = spl[3]
            return spl[1], remote.strip('[:]') if remote.startswith('[') else None
        error('Cannot get current branch in "{}".'.format(pth))
        return None, None

    @classmethod
    @log(
        'Checking for merge conflicts in {files}...',
        based_on_return=True,
        true='There are merge conflicts in {files}.',
        false=(AS_INFO, 'No merge conflicts in {files}.'),
    )
    def merge_conflicts(cls, files):
        for pth in files:
            try:
                with open(pth, 'rb') as fd:
                    data = fd.read()
            except (OSError, IOError) as e:
                error('Cannot check for merge conflicts in "{}": {}.'.format(pth, e))
                return True
            if '\n<<<<<<<' in data or '\n>>>>>>>' in data or '\n=======' in data:
                error('Please resolve merge conflicts in "{}".'.format(pth))
                return True

    @classmethod
    @log(
        'Resolving merge conflicts in "{path}"...',
        'Done resolving merge conflicts in "{path}".',
    )
    def resolve_merge_conflicts(cls, path):
        out = cmd('git diff --name-only --diff-filter=U', cwd=path, output=True)
        for pth in {join_paths(path, line) for line in out.splitlines()}:
            info('Resolving merge conflicts in "{}"...'.format(pth))
            try:
                with open(pth, 'r+') as fd:
                    data = fd.read()
                    data, c1 = mc_pattern_repl1('', data)
                    data, c2 = mc_pattern_repl2('', data)
                    if c1 + c2 == 0:
                        continue
                    fd.seek(0)
                    fd.truncate()
                    fd.write(data)
            except (OSError, IOError) as e:
                error('Cannot process conflicted file "{}": {}'.format(pth, e))
                continue
            info('Resolved merge conflicts in "{}".'.format(pth))
            curdir, f = split(pth)
            if cmd('git add {file}', file=f, cwd=curdir):
                info('Added "{}" to index after resolving its merge conflicts.'.format(pth))
            else:
                error('Cannot add "{}" to index after resolving its merge conflicts.'.format(pth))

    @classmethod
    def unstash(cls, pth):
        if not cmd('git stash list', cwd=pth, output=True):
            return
        if not cmd('git stash apply', cwd=pth):
            info('Resolving merge conflicts that occurred during stash apply...')
            GitCommands.resolve_merge_conflicts(pth)
        cmd('git stash drop', cwd=pth)

    @classmethod
    def reset(cls, pth, base=''):
        cmd('git reset --hard {base}', base=base, cwd=pth)
        cmd('git clean -df', cwd=pth)

    @classmethod
    @log('Remote data fetch in "{pth}".')
    def fetch(cls, pth):
        cmd('git fetch origin', cwd=pth)
        cmd('git remote prune origin', cwd=pth)

    @classmethod
    def IS_VALID(cls, pth):
        if pth.lower().startswith('http'):
            return
        return cmd('git ls-remote {pth} HEAD', pth=pth)

    def DOWNLOAD(self, pth):
        cmd('git clone --recursive {url} {pth}', url=self.trunk.url, pth=pth)
        cmd('git checkout -B {branch} {base}', branch=FIX, base=self.trunk.branch, cwd=pth)
        cmd('git submodule update --init --recursive', cwd=pth)

    @log('{self.__class__.__name__}: Update of "{self.trunk.last_rev}" ({self.trunk.url}).')
    def UPDATE(self):
        url = self.trunk.url
        pth = self.trunk.last_rev
        DEV = self.trunk.branch

        # fetch all the changes, just for the sake of completeness
        self.fetch(pth)

        curbr, rembr = self.current_branch(pth)
        # ensure that fix branch is active
        if curbr != FIX:
            cmd('git stash', cwd=pth)
            cmd('git checkout -B {branch} {base}', branch=FIX, base=DEV, cwd=pth)
            self.unstash(pth)
            locals()['FIX'] = FIX
            info(
                'Current branch in "{pth}" ({url}) is changed from "{curbr}" to "{FIX}" '
                'and tracks "{DEV}".'.format(**locals())
            )
        # ensure that current branch tracks correct remote
        if rembr != DEV:
            cmd('git branch -u {base}', base=DEV, cwd=pth)

        # collect modified files and return if no changes
        # it may occur that no changes can be found but branch is still behind dev branch
        # if any issues occur, remove the check for empty changes
        out = cmd('git diff --name-status HEAD {base}', base=DEV, cwd=pth, output=True)
        if not out:
            info('No changes in "{pth}" ({url}) against "{DEV}".'.format(**locals()))
            return

        # stash any current changes to ensure successful rebase
        # do not use rebase --autostash, as unstash failure cannot be handled
        cmd('git stash', cwd=pth)

        # reset all ahead commits
        self.reset(pth, DEV)

        # rebase onto dev branch
        info('Rebasing "{pth}" ({url})...'.format(**locals()))
        res = cmd('git rebase -X ours {base}', base=DEV, cwd=pth)
        if not res:
            info('Cannot rebase "{pth}" ({url}).'.format(**locals()))
            self.resolve_merge_conflicts(pth)
            info('Retrying rebase of "{pth}" ({url})...'.format(**locals()))
            res = cmd('git rebase -X ours {base}', base=DEV, cwd=pth)
        # log rebase result
        if res:
            info('Successfully rebased "{pth}" ({url}).'.format(**locals()))
        else:
            error('Cannot rebase "{pth}" ({url}).'.format(**locals()))
            return

        # restore and merge stashed changes if any
        self.unstash(pth)

        # update external modules
        cmd('git submodule update --init --recursive', cwd=pth)

        # yield modified files with their states
        for x in (line.split() for line in out.splitlines()):
            p = join_paths(pth, x[-1])
            if isfile(p):
                yield x[0], p

    @log(
        '{self.__class__.__name__}: Committing {files}...',
        '{self.__class__.__name__}: Commit result for {files}: {__result__}',
    )
    def COMMIT(self, files, message, update_first=True):
        if not files:
            error('No files are specified for commit.')
            return {'result': False}

        # get repository dir
        cwd, _ = split(files[0])

        changes = set()
        if update_first:
            changes = {pth for _, pth in self.UPDATE()}

        # ensure that there are no merge conflicts in files
        if self.merge_conflicts(files):
            return {
                'changes': changes,
                'result': False,
            }

        # combine all local commits to one commit
        # get number of ahead commits
        count = len(cmd('git rev-list @{{u}}..', cwd=cwd, output=True).splitlines())
        # reset ahead commits
        cmd('git reset HEAD~{count}', count=count, cwd=cwd)
        # add files to index
        for pth in files:
            curdir, f = split(pth)
            cmd('git add {file}', file=f, cwd=curdir)
        # commit files
        if not cmd('git commit -m "{message}"', message=message or 'Texts reviewed', cwd=cwd):
            error('Failed to commit {}.'.format(files))
            cmd('git reset', cwd=cwd)
            return {
                'changes': changes,
                'result': False,
            }
        commit_id = cmd('git rev-parse HEAD', cwd=cwd, output=True)

        # force-push commit to remote fix branch
        out = cmd('git push -f origin {branch}', branch=FIX, cwd=cwd, output=True, err2out=True)
        logstr = 'Pushing changes to remote server...'
        info('\n'.join([logstr, out, '=' * len(logstr)]))
        # reset commit as it is not yet in dev branch
        cmd('git reset HEAD~1', cwd=cwd)

        return {
            'changes': set(files) | changes,
            'result': bool(out),
            'id': commit_id,
        }

    def REVERT(self, pth, isfile=True):
        if isfile:
            cwd, f = split(pth)
            cmd('git checkout -- {file}', file=f, cwd=cwd)
        else:
            self.reset(pth)

    def GET_FILE_LASTREV(self, pth, dest):
        cwd, f = split(pth)
        out = cmd('git show {branch}:./{file}', branch=FIX, file=f, cwd=cwd, output=True)
        if not out:
            return
        try:
            with open(dest, 'w') as fd:
                fd.write(out)
            return True
        except (OSError, IOError) as e:
            error('Cannot write to file: {}'.format(e))

    def MODIFIED(self, pth):
        CMD = 'git ls-files --modified'

        if isdir(pth):
            out = cmd(CMD, cwd=pth, output=True)
            return {join_paths(pth, line) for line in out.splitlines()}

        cwd, f = split(pth)
        out = cmd(CMD, cwd=cwd, output=True)
        return f in set(out.splitlines())

    def REVIEW_BRANCHES(self):
        return {
            'source': FIX,
            'target': self.trunk.branch.replace('origin/', '', 1),
        }

    def LIST_BRANCHES(self):
        pth = self.trunk.last_rev
        self.fetch(pth)
        return [
            br.strip()
            for br in cmd('git branch -r', cwd=pth, output=True).splitlines()
            if '->' not in br
        ]

    @staticmethod
    def __parseurl(st, splitter, right=False):
        parsed = st.split(splitter, 1)
        return parsed[int(right)] if len(parsed) > 1 else parsed[0]

    # TODO: bad style, refactor
    # need to move URL parsing logic to REST backend
    # URL string must be returned from REST
    def URL(self, pth):
        from ..bindings import VCS_REVIEW_BINDING
        from ..cmdline import CMD_ARGS

        rest = VCS_REVIEW_BINDING[self].rest
        if rest is None:
            error('Cannot get URL for "{}" because no REST API class is bound to "{}".'.format(
                pth, self.__class__.__name__
            ))
            return
        url = self.trunk.url
        spl = url.split('/')
        if len(spl) < 3:
            error('"{}" is not a valid URL.'.format(url))
            return
        srv = self.__parseurl(spl[2], '@', True)
        srv = 'https://' + self.__parseurl(srv, ':')
        with rest(srv, (CMD_ARGS['USER'], CMD_ARGS['PASS']), url) as session:
            rep = session.repository
        if not rep['valid']:
            error(
                'Cannot get URL for "{}" because no information about '
                'remote repository is retrieved.'.format(pth)
            )
            return
        return '{srv}/projects/{origin_key}/repos/{origin_slug}/browse'.format(**rep) + \
            self.trunk.shorten(pth)

    def COMMAND(self, cmd_str, pth):
        cwd = pth if isdir(pth) else split(pth)[0]
        self.fetch(cwd)
        cmd(cmd_str.format(pth), cwd=cwd, hide_wnd=False)
