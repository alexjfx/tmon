class Commands:

    name = None
    implemented = {
        'LIST_BRANCHES': False,
        'COMMIT': False,
    }

    @classmethod
    def IS_VALID(cls, pth):
        return True

    def __init__(self, trunk=None):
        self.trunk = trunk

    def DOWNLOAD(self, pth):
        return

    def UPDATE(self):
        return []

    def COMMIT(self, files, message, update_first=True):
        return {
            'changes': [],
            'result': None,
            'id': None,
        }

    def REVERT(self, pth, isfile=True):
        return

    def GET_FILE_LASTREV(self, pth, dest):
        return False

    def MODIFIED(self, pth):
        return []

    def COMMITTED(self, pth, rev_id):
        return False

    def REVIEW_BRANCHES(self):
        return {
            'source': '',
            'target': '',
        }

    def LIST_BRANCHES(self):
        return []

    def URL(self, pth):
        return

    def COMMAND(self, cmd_str, pth):
        return
