from _winreg import *
from logging import error


class Registry:

    def __init__(self, key, root=HKEY_CURRENT_USER):
        self.__call__(key, root)

    def __call__(self, key, root=HKEY_CURRENT_USER):
        self.close()
        try:
            self.key = OpenKey(root, key, 0, KEY_ALL_ACCESS)
        except OSError as e:
            error('Cannot open registry key "{}": {}'.format(key, e))
            self.key = None
        return self

    def exists(self, value_name):
        try:
            return bool(QueryValueEx(self.key, value_name))
        except (OSError, TypeError):
            return False

    def get(self, value_name):
        try:
            return QueryValueEx(self.key, value_name)
        except (OSError, TypeError) as e:
            error('Cannot get registry value "{}": {}'.format(value_name, e))

    def set(self, value_name, data, typ=REG_SZ):
        try:
            SetValueEx(self.key, value_name, 0, typ, data)
        except (OSError, TypeError) as e:
            error('Cannot set registry value "{}": {}'.format(value_name, e))

    def delete(self, value_name):
        try:
            DeleteValue(self.key, value_name)
        except (OSError, TypeError):
            pass

    def close(self):
        if getattr(self, 'key', None) is None:
            return
        CloseKey(self.key)
        self.key = None
