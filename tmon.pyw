import wx

from lib.common import APP_DIR
from lib.gui.main_frame import MainFrame
from lib.logger import init_logger
from logging import WARNING
from logging import getLogger
from multiprocessing import freeze_support
from os.path import basename

getLogger('requests').setLevel(WARNING)
getLogger('urllib3').setLevel(WARNING)


class SingleApp(wx.App):

    def OnInit(self):
        self.name = '{}-{}'.format(basename(__file__), wx.GetUserId())
        self.instance = wx.SingleInstanceChecker(self.name)
        if self.instance.IsAnotherRunning():
            return False
        return True


if __name__ == '__main__':
    freeze_support()
    init_logger(APP_DIR + '/debug.log')
    app = SingleApp()
    frame = MainFrame(None)
    frame.Show()
    app.MainLoop()
